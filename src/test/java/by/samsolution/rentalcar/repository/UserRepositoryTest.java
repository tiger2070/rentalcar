package by.samsolution.rentalcar.repository;

import by.samsolution.rentalcar.entity.User;
import org.junit.After;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class UserRepositoryTest extends AbstractRepositoryTest<User> {
    @Resource
    private UserRepository userRepository;

    @Override
    protected User getNewEntity() {
        return new User();
    }

    @After
    public void deInit() {
        userRepository.deleteAll();
    }

    @Test
    public void createTest() {
        entity.setLogin("User123");
        Integer id = userRepository.save(entity).getId();
        User savedUser = userRepository.findOne(id);
        assertEquals(entity.getLogin(), savedUser.getLogin());
    }

    @Test

    public void updateTest() {
        Integer id = userRepository.save(entity).getId();
        entity.setLogin("Admin777");
        userRepository.save(entity);
        User user = userRepository.findOne(id);
        assertEquals(entity.getLogin(), user.getLogin());
    }


    @Test
    public void saveAllTest() {
        List<User> userList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            userList.add(new User());
        }
        userRepository.save(userList);
        assertEquals(userList.size(), userRepository.findAll().size());
    }

    @Test
    public void deleteTest() {
        Integer id = userRepository.save(entity).getId();
        userRepository.delete(id);
        assertEquals(null, userRepository.findOne(id));
    }


}
