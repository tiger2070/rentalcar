package by.samsolution.rentalcar.repository;

import by.samsolution.rentalcar.entity.CarClass;
import org.junit.After;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class CarClassRepositoryTest extends AbstractRepositoryTest<CarClass> {
    @Resource
    private CarClassRepository carClassRepository;

    @Override
    protected CarClass getNewEntity() {
        return new CarClass();
    }

    @After
    public void deInit() {
        carClassRepository.deleteAll();
    }

    @Test
    public void createTest() {
        entity.setClassType("SUV");
        Integer id = carClassRepository.save(entity).getId();
        CarClass carClass = carClassRepository.findOne(id);
        assertEquals(entity.getClassType(), carClass.getClassType());
    }

    @Test
    public void updateTest() {
        Integer id = carClassRepository.save(entity).getId();
        entity.setClassType("Medium");
        carClassRepository.save(entity);
        CarClass carClass = carClassRepository.findOne(id);
        assertEquals(entity.getClassType(), carClass.getClassType());
    }

    @Test
    public void deleteTest() {
        Integer id = carClassRepository.save(entity).getId();
        carClassRepository.delete(id);
        assertEquals(null, carClassRepository.findOne(id));
    }

    @Test
    public void findAllTest() {
        List<CarClass> carClassList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            carClassList.add(new CarClass());
        }
        carClassRepository.save(carClassList);
        assertEquals(10, carClassRepository.findAll().size());
    }
}
