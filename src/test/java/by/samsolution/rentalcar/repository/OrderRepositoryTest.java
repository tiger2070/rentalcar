package by.samsolution.rentalcar.repository;


import by.samsolution.rentalcar.entity.Order;
import org.junit.After;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class OrderRepositoryTest extends AbstractRepositoryTest<Order> {

    @Resource
    private OrderRepository orderRepository;

    @Override
    protected Order getNewEntity() {
        return new Order();
    }

    @After
    public void deInit() {
        orderRepository.deleteAll();
    }

    @Test
    public void deleteTest() {
        Integer id = orderRepository.save(entity).getId();
        orderRepository.delete(id);
        assertEquals(orderRepository.findOne(id), null);
    }


    @Test
    public void findAllTest() {
        Order order = new Order();
        orderRepository.save(order);
        orderRepository.save(this.entity);
        assertEquals(2, orderRepository.findAll().size());
    }

    @Test
    public void deleteAllTest() {
        List<Order> orderList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            orderList.add(new Order());
        }
        orderRepository.save(orderList);
        orderRepository.deleteAll();
        assertEquals(0, orderRepository.findAll().size());

    }

    @Test
    public void saveAllTest() {
        List<Order> orderList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            orderList.add(new Order());
        }
        orderRepository.save(orderList);
        assertEquals(10, orderRepository.findAll().size());
    }


}
