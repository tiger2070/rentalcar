package by.samsolution.rentalcar.repository;

import by.samsolution.rentalcar.entity.Car;
import by.samsolution.rentalcar.entity.Order;
import by.samsolution.rentalcar.entity.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;

public class OrderRepositoryCreateUpdateTest extends AbstractRepositoryTest<Order> {
    @Resource
    private OrderRepository orderRepository;
    @Resource
    private CarRepository carRepository;
    @Resource
    private UserRepository userRepository;

    @Override
    protected Order getNewEntity() {
        return new Order();
    }

    @Before
    public void init() {
        User user = userRepository.save(new User());
        Car car = carRepository.save(new Car());
        entity.setCar(car);
        entity.setUser(user);
    }

    @After
    public void deInit() {
        orderRepository.deleteAll();
    }

    @Test
    public void createTest() {
        entity.setTotalCost(999.0);
        Integer id = orderRepository.save(entity).getId();
        Order createdOrder = orderRepository.findOne(id);
        assertEquals(entity.getTotalCost(), createdOrder.getTotalCost(), 0);
    }

    @Test
    public void updateTest() {
        Integer id = orderRepository.save(entity).getId();
        entity.setTotalCost(777.0);
        orderRepository.save(entity);
        Order updatedOrder = orderRepository.findOne(id);
        assertEquals(entity.getTotalCost(), updatedOrder.getTotalCost(), 0);
    }


}
