package by.samsolution.rentalcar.repository;


import by.samsolution.rentalcar.entity.Engine;
import org.junit.After;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class EngineRepositoryTest extends AbstractRepositoryTest<Engine> {
    @Resource
    private EngineRepository engineRepository;

    @Override
    protected Engine getNewEntity() {
        return new Engine();
    }

    @After
    public void deInit() {
        engineRepository.deleteAll();
    }

    @Test
    public void createTest() {
        entity.setEngineType("benzine");
        Integer id = engineRepository.save(entity).getId();
        Engine savedEngine = engineRepository.findOne(id);
        assertEquals(entity.getEngineType(), savedEngine.getEngineType());
    }

    @Test
    public void updateTest() {
        Integer id = engineRepository.save(entity).getId();
        entity.setEngineType("diesel");
        engineRepository.save(entity);
        Engine savedEngine = engineRepository.findOne(id);
        assertEquals(entity.getEngineType(), savedEngine.getEngineType());
    }

    @Test
    public void deleteTest() {
        Integer id = engineRepository.save(entity).getId();
        engineRepository.delete(id);
        assertEquals(null, engineRepository.findOne(id));
    }

    @Test
    public void findAllTest() {
        List<Engine> engineList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            engineList.add(new Engine());
        }
        engineRepository.save(engineList);
        assertEquals(10, engineRepository.findAll().size());
    }
}
