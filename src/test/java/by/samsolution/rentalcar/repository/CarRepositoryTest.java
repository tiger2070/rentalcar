package by.samsolution.rentalcar.repository;

import by.samsolution.rentalcar.entity.Car;
import org.junit.After;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class CarRepositoryTest extends AbstractRepositoryTest<Car> {

    @Resource
    private CarRepository carRepository;

    @Override
    protected Car getNewEntity() {
        return new Car();
    }

    @After
    public void deInit() {
        carRepository.deleteAll();
    }

    @Test
    public void createTest() {
        entity.setMark("Ford");
        Integer id = carRepository.save(entity).getId();
        Car createdCar = carRepository.findOne(id);
        assertEquals(entity.getMark(), createdCar.getMark());
    }

    @Test
    public void deleteTest() {
        Integer id = carRepository.save(entity).getId();
        carRepository.delete(id);
        assertEquals(carRepository.findOne(id), null);
    }

    @Test
    public void updateTest() {
        carRepository.save(entity);
        entity.setMark("Ferrari");
        carRepository.save(entity);
        Car updatedCar = carRepository.findOne(entity.getId());
        assertEquals(updatedCar.getMark(), entity.getMark());
    }

    @Test
    public void findAllTest() {
        Car car1 = new Car();
        carRepository.save(car1);
        carRepository.save(entity);
        assertEquals(2, carRepository.findAll().size());
    }

    @Test
    public void deleteAllTest() {
        List<Car> carList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            carList.add(new Car());
        }
        carRepository.save(carList);
        carRepository.deleteAll();
        assertEquals(0, carRepository.findAll().size());
    }

    @Test
    public void saveAllTest() {
        List<Car> carList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            carList.add(new Car());
        }
        carRepository.save(carList);
        assertEquals(10, carRepository.findAll().size());
    }

    @Test(expected = org.springframework.orm.jpa.JpaOptimisticLockingFailureException.class)
    public void updateOptimisticLockFailureTest() {
        Car saved = carRepository.save(entity);
        Car entityToUpdate = carRepository.findOne(saved.getId());
        Car entityToUpdateTwo = carRepository.findOne(saved.getId());
        entityToUpdate.setMark("Ferrari");
        carRepository.save(entityToUpdate);
        entityToUpdateTwo.setMark("Honda");
        carRepository.save(entityToUpdateTwo);
    }


}
