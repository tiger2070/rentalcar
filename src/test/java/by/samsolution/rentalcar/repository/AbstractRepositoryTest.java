package by.samsolution.rentalcar.repository;

import by.samsolution.rentalcar.config.WebMvcConfigTest;
import by.samsolution.rentalcar.entity.IEntity;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebMvcConfigTest.class)
@WebAppConfiguration

public abstract class AbstractRepositoryTest<T extends IEntity> {

    protected T entity;

    @Before
    public void initEntity() {
        entity = getNewEntity();
    }

    @After
    public void deInitEntity() {
        entity = null;
    }

    protected abstract T getNewEntity();

}
