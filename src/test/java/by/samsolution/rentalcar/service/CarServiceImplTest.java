package by.samsolution.rentalcar.service;

import by.samsolution.rentalcar.entity.Car;
import by.samsolution.rentalcar.repository.CarRepository;
import by.samsolution.rentalcar.service.impl.CarServiceImpl;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import javax.annotation.Resource;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CarServiceImplTest extends AbstractServiceTest {
    @Resource
    @InjectMocks
    private CarServiceImpl carServiceImpl;
    @Mock
    private CarRepository carRepository;

    @Test
    public void deleteCarTest() {
        Car foundCar = new Car();
        when(carRepository.findOne(anyInt())).thenReturn(foundCar);
        carServiceImpl.delete(8);
        verify(carRepository).delete(foundCar);
    }
}
