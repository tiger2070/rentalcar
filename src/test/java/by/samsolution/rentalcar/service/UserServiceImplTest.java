package by.samsolution.rentalcar.service;

import by.samsolution.rentalcar.entity.User;
import by.samsolution.rentalcar.exception.LoginAlreadyExistException;
import by.samsolution.rentalcar.repository.UserRepository;
import by.samsolution.rentalcar.service.impl.UserServiceImpl;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.Resource;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserServiceImplTest extends AbstractServiceTest {
    @InjectMocks
    @Resource
    private UserServiceImpl userServiceImpl;

    @Mock
    private UserRepository userRepository;
    @Mock
    PasswordEncoder bCryptPasswordEncoder;

    @Test(expected = LoginAlreadyExistException.class)
    public void createUserExceptionTest() {
        User user = new User();
        when(userRepository.getUser(anyString())).thenReturn(new User());
        userServiceImpl.create(user);
    }

    @Test
    public void createUserTest() {
        User user = new User();
        when(userRepository.getUser(anyString())).thenReturn(null);
        when(bCryptPasswordEncoder.encode(anyString())).thenReturn(anyString());
        userServiceImpl.create(user);
        verify(userRepository).save(user);
    }

    @Test
    public void updateUserTest() {
        User user = new User();
        when(bCryptPasswordEncoder.encode(anyString())).thenReturn(anyString());
        when(userRepository.save(user)).thenReturn(user);
        userServiceImpl.update(user);
        verify(userRepository).save(user);
    }

}
