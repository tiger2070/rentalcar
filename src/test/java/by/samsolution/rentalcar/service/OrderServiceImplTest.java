package by.samsolution.rentalcar.service;


import by.samsolution.rentalcar.entity.Car;
import by.samsolution.rentalcar.entity.Order;
import by.samsolution.rentalcar.exception.CarIsBusyException;
import by.samsolution.rentalcar.repository.OrderRepository;
import by.samsolution.rentalcar.service.impl.OrderServiceImpl;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

public class OrderServiceImplTest extends AbstractServiceTest {

    @Resource
    @InjectMocks
    private OrderServiceImpl orderServiceImpl;
    @Mock
    private OrderRepository orderRepository;

    @Test
    public void createOrderTest() {
        List<Order> orderList = new ArrayList<>();
        Order order = mock(Order.class);
        Car car = mock(Car.class);
        when(order.getCar()).thenReturn(car);
        when(orderRepository.checkAvailableCarForOrderCreate(order.getCar().getId(), order.getDateIn(), order.getDateOut())).thenReturn(orderList);
        orderServiceImpl.create(order);
        verify(orderRepository).save(order);

    }

    @Test(expected = CarIsBusyException.class)
    public void createOrderCarIsBusyExceptionTest() {
        List<Order> orderList = new ArrayList<>();
        orderList.add(new Order());
        Car car = new Car();
        Order order = new Order();
        order.setCar(car);
        when(orderRepository.checkAvailableCarForOrderCreate(car.getId(), order.getDateIn(), order.getDateOut())).thenReturn(orderList);
        orderServiceImpl.create(order);
    }

    @Test
    public void updateOrderTest() {
        Order order = new Order();
        Order orderFromDB = mock(Order.class);
        Car car = mock(Car.class);
        when(orderFromDB.getCar()).thenReturn(car);
        when(orderRepository.findOne(anyInt())).thenReturn(orderFromDB);
        orderServiceImpl.update(order);
        verify(orderRepository).save(order);

    }


}
