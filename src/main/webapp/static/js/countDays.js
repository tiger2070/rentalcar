var date1, date2, dateTo, dateFrom, days;
// parse a date in yyyy-mm-dd format
function parseDate(input) {
    var parts = input.split('.');
    // new Date(year, month [, day [, hours[, minutes[, seconds[, ms]]]]])
    return new Date(parts[2], parts[1] - 1, parts[0]); // Note: months are 0-based
}


function days_between(dateTo, dateFrom) {

    // The number of milliseconds in one day
    var ONE_DAY = 1000 * 60 * 60 * 24;

    date1 = parseDate(dateTo);
    date2 = parseDate(dateFrom);

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    var difference_ms = Math.abs(date1_ms - date2_ms);

    // Convert back to days and return
    return Math.round(difference_ms / ONE_DAY);
};

$("document").ready(function () {
    $('#TxtFromDate').change(function () {
        dateTo = $("#TxtFromDate").val();
        console.log(dateTo);
    });
    $('#TxtToDate').change(function () {
        dateFrom = $("#TxtToDate").val();
        console.log(dateFrom);
        days = days_between(dateTo, dateFrom);
        var costPerDay = $("#costPerDay").val();
        console.log(costPerDay);


        var sum = costPerDay * days;
        $("#cost").val(sum);
    });
});





