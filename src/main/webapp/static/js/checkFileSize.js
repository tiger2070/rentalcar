function checkFileSize(inputFile) {
    var max = 10 * 1024 * 1024; // 10MB

    if (inputFile.files && inputFile.files[0].size > max) {
        alert("File is larger than 10MB."); // Do your thing to handle the error.
        inputFile.value = null; // Clear the field.
    }
}
