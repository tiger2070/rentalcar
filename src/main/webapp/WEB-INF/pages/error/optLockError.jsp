<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isErrorPage="true" %>
<html>
<head>
    <title>Optimistic lock error</title>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <link href="${pageContext.request.contextPath}/static/css/bootstrap.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/static/css/custom.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/static/img/fav.png" rel="shortcut icon"/>
    <script src="${pageContext.request.contextPath}/static/js/jquery-2.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/respond.js"></script>
</head>
<body>
<div class="container">
    <!--row 1-->
    <jsp:include page="/WEB-INF/pages/fragments/header.jsp"/>

    <!--row 2-->
    <jsp:include page="/WEB-INF/pages/fragments/menu.jsp"/>

    <%--row3--%>
    <div class="content row">
        <aside class="col-xs-2">
            <jsp:include page="/WEB-INF/pages/fragments/carclasses.jsp"/>
        </aside>

        <article class="col-xs-10">
            <p>

            <h3><spring:message code="optlock.message1" text="missing"/>.</h3></p>
            <p>

            <h3><spring:message code="optlock.message2" text="missing"/>.</h3></p>
        </article>
    </div>

    <%--row4--%>
    <jsp:include page="/WEB-INF/pages/fragments/footer.jsp"/>

</div>


</body>
</html>
