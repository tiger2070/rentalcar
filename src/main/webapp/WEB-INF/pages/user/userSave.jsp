<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>
        <c:choose>
            <c:when test="${entity.id != null}">
                <spring:message code="change.details" text="missing"/>
            </c:when>
            <c:when test="${entity.id == null}">
                <spring:message code="registration.new.user" text="missing"/>
            </c:when>
        </c:choose>
    </title>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <link href="${pageContext.request.contextPath}/static/css/bootstrap.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/static/css/custom.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/static/img/fav.png" rel="shortcut icon"/>
    <script src="${pageContext.request.contextPath}/static/js/jquery-2.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/respond.js"></script>
</head>
<body>
<div class="container">
<!--row 1-->
<jsp:include page="/WEB-INF/pages/fragments/header.jsp"/>

<!--row 2-->
<jsp:include page="/WEB-INF/pages/fragments/menu.jsp"/>

<%--row3--%>
<div class="content row">
<aside class="col-xs-2">
    <jsp:include page="/WEB-INF/pages/fragments/carclasses.jsp"/>
</aside>

<article class="col-xs-10">
<h2>
    <c:choose>
        <c:when test="${entity.id != null}">
            <spring:message code="change.details" text="missing"/>
        </c:when>
        <c:when test="${entity.id == null}">
            <spring:message code="registration.new.user" text="missing"/>
        </c:when>
    </c:choose>
</h2>
<c:set var="save">
    <c:if test="${entity.id==null}">${pageContext.request.contextPath}/user/create</c:if>
    <c:if test="${entity.id!=null}">${pageContext.request.contextPath}/user/update/${entity.id}</c:if>
</c:set>
<form:form method="post" commandName="entity" action="${save}" enctype="multipart/form-data">
<form:hidden path="id"></form:hidden>
<form:hidden path="version"></form:hidden>
<sec:authorize access="!hasAuthority('ADMIN')">
    <form:hidden path="accessLevel"></form:hidden>
</sec:authorize>
<c:if test="${loginAlreadyExist != null}">
    <p class="error"><spring:message code="login.already.exists" text="missing"/></p>
</c:if>
<table class="fashion-table">
<tr>
    <th></th>
    <th><spring:message code="personal.details" text="missing"/></th>
</tr>
<tr>
    <sec:authorize access="!hasAuthority('ADMIN')" var="noAdmin"/>
    <td><form:label path="login"><spring:message code="login" text="missing"/></form:label></td>
    <td>
        <c:choose>
            <c:when test="${noAdmin}">
                <c:if test="${entity.id!=null}">
                    <form:input path="login" readonly="true" cssClass="read-only"/>
                </c:if>
                <c:if test="${entity.id==null}">
                    <form:input path="login" readonly="false"/>
                </c:if>
            </c:when>
            <c:otherwise>
                <form:input path="login" readonly="false"/>
            </c:otherwise>
        </c:choose>
    </td>
    <td class="td-unborder"><form:errors path="login" cssClass="error"/></td>
</tr>
<tr>
    <td><form:label path="password"><spring:message code="password" text="missing"/></form:label></td>
    <td><form:password path="password" readonly="false"/></td>
    <td><form:errors path="password" cssClass="error"/></td>
</tr>
<tr>
    <td><form:label path="confirmPassword"><spring:message code="confirm.password"
                                                           text="missing"/></form:label></td>
    <td><form:password path="confirmPassword"/></td>
    <td><form:errors path="confirmPassword" cssClass="error"/></td>
</tr>
<tr>
    <td><form:label path="email">Email</form:label></td>
    <td><form:input path="email"/></td>
    <td><form:errors path="email" cssClass="error"/></td>
</tr>
<tr>
    <td><form:label path="firstName"><spring:message code="firstname" text="missing"/></form:label></td>
    <td>
        <c:choose>
            <c:when test="${noAdmin}">
                <c:if test="${entity.id!=null}">
                    <form:input path="firstName" readonly="true" cssClass="read-only"/>
                </c:if>
                <c:if test="${entity.id==null}">
                    <form:input path="firstName" readonly="false"/>
                </c:if>
            </c:when>
            <c:otherwise>
                <form:input path="firstName" readonly="false"/>
            </c:otherwise>
        </c:choose>
    </td>
    <td><form:errors path="firstName" cssClass="error"/></td>
</tr>
<tr>
    <td><form:label path="lastName"><spring:message code="lastname" text="missing"/></form:label></td>
    <td>
        <c:choose>
            <c:when test="${noAdmin}">
                <c:if test="${entity.id!=null}">
                    <form:input path="lastName" readonly="true" cssClass="read-only"/>
                </c:if>
                <c:if test="${entity.id==null}">
                    <form:input path="lastName" readonly="false"/>
                </c:if>
            </c:when>
            <c:otherwise>
                <form:input path="lastName" readonly="false"/>
            </c:otherwise>
        </c:choose>
    </td>
    <td><form:errors path="lastName" cssClass="error"/></td>
</tr>
<tr>
    <td><form:label path="birthday"><spring:message code="date.birth" text="missing"/></form:label></td>
    <td>
        <c:choose>
            <c:when test="${noAdmin}">
                <c:if test="${entity.id!=null}">
                    <form:input path="birthday" readonly="true" cssClass="read-only"/>
                </c:if>
                <c:if test="${entity.id==null}">
                    <form:input path="birthday" readonly="false"/>
                </c:if>
            </c:when>
            <c:otherwise>
                <form:input path="birthday" readonly="false"/>
            </c:otherwise>
        </c:choose>
    </td>
    <td><form:errors path="birthday" cssClass="error"/></td>
</tr>
<tr>
    <td><form:label path="phone"><spring:message code="phone" text="missing"/></form:label></td>
    <td><form:input path="phone"/></td>
    <td><form:errors path="phone" cssClass="error"/></td>
</tr>
<sec:authorize access="hasAuthority('ADMIN')">
    <tr>
        <td><form:label path="accessLevel"><spring:message code="acceslevel"/></form:label></td>
        <td><form:select path="accessLevel">
            <form:option value="ADMIN"><spring:message code="admin" text="missing"/></form:option>
            <form:option value="CLIENT"><spring:message code="client" text="missing"/></form:option>
        </form:select></td>
    </tr>
</sec:authorize>
<tr>
    <th></th>
    <th><spring:message code="passport.details" text="missing"/></th>
</tr>
<tr>
    <td><form:label path="passport.seriesAndNumber"><spring:message code="number"
                                                                    text="missing"/></form:label></td>

    <td>
        <c:choose>
            <c:when test="${noAdmin}">
                <c:if test="${entity.id!=null}">
                    <form:input path="passport.seriesAndNumber" readonly="true" cssClass="read-only"/>
                </c:if>
                <c:if test="${entity.id==null}">
                    <form:input path="passport.seriesAndNumber" readonly="false"/>
                </c:if>
            </c:when>
            <c:otherwise>
                <form:input path="passport.seriesAndNumber" readonly="false"/>
            </c:otherwise>
        </c:choose>
    </td>
    <td><form:errors path="passport.seriesAndNumber" cssClass="error"/></td>
</tr>
<tr>
    <td><form:label path="passport.givingDate"><spring:message code="date.of.issue"
                                                               text="missing"/></form:label></td>
    <td>
        <c:choose>
            <c:when test="${noAdmin}">
                <c:if test="${entity.id!=null}">
                    <form:input path="passport.givingDate" readonly="true" cssClass="read-only"/>
                </c:if>
                <c:if test="${entity.id==null}">
                    <form:input path="passport.givingDate" readonly="false"/>
                </c:if>
            </c:when>
            <c:otherwise>
                <form:input path="passport.givingDate" readonly="false"/>
            </c:otherwise>
        </c:choose>
    </td>
    <td><form:errors path="passport.givingDate" cssClass="error"/></td>
</tr>
<tr>
    <td><form:label path="passport.expiryDate"><spring:message code="date.of.expiry"
                                                               text="missing"/></form:label></td>
    <td>
        <c:choose>
            <c:when test="${noAdmin}">
                <c:if test="${entity.id!=null}">
                    <form:input path="passport.expiryDate" readonly="true" cssClass="read-only"/>
                </c:if>
                <c:if test="${entity.id==null}">
                    <form:input path="passport.expiryDate" readonly="false"/>
                </c:if>
            </c:when>
            <c:otherwise>
                <form:input path="passport.expiryDate" readonly="false"/>
            </c:otherwise>
        </c:choose>
    </td>
    <td><form:errors path="passport.expiryDate" cssClass="error"/></td>
</tr>
<tr>
    <td><form:label path="passport.issuedBy"><spring:message code="place.of.issue"
                                                             text="missing"/></form:label></td>
    <td>
        <c:choose>
            <c:when test="${noAdmin}">
                <c:if test="${entity.id!=null}">
                    <form:input path="passport.issuedBy" readonly="true" cssClass="read-only"/>
                </c:if>
                <c:if test="${entity.id==null}">
                    <form:input path="passport.issuedBy" readonly="false"/>
                </c:if>
            </c:when>
            <c:otherwise>
                <form:input path="passport.issuedBy" readonly="false"/>
            </c:otherwise>
        </c:choose>
    </td>
    <td><form:errors path="passport.issuedBy" cssClass="error"/></td>
</tr>
<tr>
    <td></td>
    <td><input class="btn btn-success btn-sm" type="submit" value="<spring:message code="save" text="missing"/>"></td>
</tr>
</table>
</form:form>
</article>
</div>

<%--row4--%>
<jsp:include page="/WEB-INF/pages/fragments/footer.jsp"/>

</div>

</body>
</html>
