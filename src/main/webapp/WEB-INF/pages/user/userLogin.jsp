<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><spring:message code="log.in"/></title>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <link href="${pageContext.request.contextPath}/static/css/bootstrap.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/static/css/custom.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/static/img/fav.png" rel="shortcut icon"/>
    <script src="${pageContext.request.contextPath}/static/js/jquery-2.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/respond.js"></script>
</head>
<body>
<div class="container">
    <!--row 1-->
    <jsp:include page="/WEB-INF/pages/fragments/header.jsp"/>

    <!--row 2-->
    <jsp:include page="/WEB-INF/pages/fragments/menu.jsp"/>

    <%--row3--%>
    <div class="content row">
        <aside class="col-xs-2">
            <jsp:include page="/WEB-INF/pages/fragments/carclasses.jsp"/>
        </aside>

        <article class="col-xs-8">

            <form class="form-signin" role="form" method="post" action="${pageContext.request.contextPath}/login"/>
            <h2 class="form-signin-heading"><spring:message code="please.sign.in"/></h2>

            <div class="login-error">
                <p><c:if test="${messageWrongPass != null}">
                    <spring:message code="Wrong.login.or.password" text="missing"/>
                </c:if></p></div>
            <input type="text" name="j_username" class="form-control" placeholder="<spring:message code="login"/>"
                   required autofocus>

            <input type="password" name="j_password" class="form-control"
                   placeholder="<spring:message code="password"/>" required/></td>
            <input type="hidden"
                   name="${_csrf.parameterName}"
                   value="${_csrf.token}"/>
            <button class="btn btn-lg btn-primary btn-block" type="submit"><spring:message code="sign.in"/></button>
            </form>
        </article>
    </div>

    <%--row4--%>
    <jsp:include page="/WEB-INF/pages/fragments/footer.jsp"/>

</div>

</body>
</html>
