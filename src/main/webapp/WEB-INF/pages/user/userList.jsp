<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <title><spring:message code="list.user" text="missing"/></title>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <link href="${pageContext.request.contextPath}/static/css/bootstrap.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/static/css/custom.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/static/img/fav.png" rel="shortcut icon"/>
    <script src="${pageContext.request.contextPath}/static/js/jquery-2.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/respond.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/deleteConfirm.js"></script>
</head>
<body>
<div class="container">
    <!--row 1-->
    <jsp:include page="/WEB-INF/pages/fragments/header.jsp"/>
    <jsp:include page="/WEB-INF/pages/fragments/deleteConfirm.jsp"/>
    <!--row 2-->
    <jsp:include page="/WEB-INF/pages/fragments/menu.jsp"/>

    <%--row3--%>
    <div class="content row">
        <aside class="col-xs-2">
            <jsp:include page="/WEB-INF/pages/fragments/carclasses.jsp"/>
        </aside>

        <article class="col-xs-10">
            <table class="fashion-table">
                <caption><h2><spring:message code="list.user" text="missing"/></h2></caption>
                <tr>
                    <th>id</th>
                    <th><spring:message code="login" text="missing"/></th>
                    <th><spring:message code="firstname" text="missing"/></th>
                    <th><spring:message code="lastname" text="missing"/></th>
                    <th><spring:message code="date.birth" text="missing"/></th>
                    <th><spring:message code="phone" text="missing"/></th>
                    <th><spring:message code="acceslevel" text="missing"/></th>
                    <th><spring:message code="action" text="missing"/></th>
                </tr>
                <c:forEach var="user" items="${page.content}">
                    <tr>
                        <td>${user.id}</td>
                        <td>
                            <a href="${pageContext.request.contextPath}/order/orders/${user.id}?page=1">${user.login}</a>
                        </td>
                        <td>${user.firstName}</td>
                        <td>${user.lastName}</td>
                        <td><fmt:formatDate value="${user.birthday}" pattern="dd.MM.yyyy"/></td>
                        <td>${user.phone}</td>
                        <td>${user.accessLevel}</td>
                        <td>
                            <a class="bg-warning"
                               href="${pageContext.request.contextPath}/user/update/${user.id}"><spring:message
                                    code="edit"
                                    text="missing"/></a>
                            <jsp:include page="/WEB-INF/pages/fragments/deleteConfirm.jsp"/>
                            <a class="bg-danger" data-confirm="<spring:message code="are.you.sure.to.delete.this?"/>"
                               href="${pageContext.request.contextPath}/user/delete/${user.id}"><spring:message
                                    code="delete"
                                    text="missing"/></a>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </article>
    </div>


    <%--row5--%>
    <c:if test="${page.totalPages > 1}">
        <div class="row">
            <div class="col-xs-12" style="text-align: center;">
                <ul class="pagination">
                    <c:forEach begin="1" var="number" end="${page.totalPages}">
                        <li><a href="${pageContext.request.contextPath}/user/list/${number}">${number}</a></li>
                    </c:forEach>
                </ul>
            </div>
        </div>
    </c:if>

    <%--row5--%>
    <jsp:include page="/WEB-INF/pages/fragments/footer.jsp"/>


</div>

</body>
</html>
