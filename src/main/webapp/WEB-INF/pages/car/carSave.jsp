<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <title><c:if test="${entity.id == null}">
        <spring:message code="add.a.car" text="missing"/>
    </c:if>
        <c:if test="${entity.id !=null}">
            <spring:message code="edit.the.car" text="missing"/>
        </c:if>
    </title>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="${pageContext.request.contextPath}/static/css/bootstrap.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/static/css/custom.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/static/img/fav.png" rel="shortcut icon"/>
    <script src="${pageContext.request.contextPath}/static/js/jquery-2.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/respond.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/checkFileSize.js"></script>
</head>
<body>
<div class="container">
    <!--row 1-->
    <jsp:include page="/WEB-INF/pages/fragments/header.jsp"/>

    <%--row2--%>
    <jsp:include page="/WEB-INF/pages/fragments/menu.jsp"/>
    <%--row3--%>

    <div class="row content">
        <aside class="col-xs-2">
            <jsp:include page="/WEB-INF/pages/fragments/carclasses.jsp"/>
        </aside>
        <article class="col-xs-10">

            <table>
                <c:set var="save">
                    <c:if test="${entity.id==null}">${pageContext.request.contextPath}/car/create</c:if>
                    <c:if test="${entity.id!=null}">${pageContext.request.contextPath}/car/update/${entity.id}</c:if>
                </c:set>
                <form:form commandName="entity" action="${save}" enctype="multipart/form-data" method="post">
                <form:hidden path="id"></form:hidden>
                <form:hidden path="version"></form:hidden>
                <tr>
                    <td></td>
                    <td><h3><c:if test="${entity.id == null}">
                        <spring:message code="add.a.car" text="missing"/>
                    </c:if>
                        <c:if test="${entity.id !=null}">
                            <spring:message code="edit.the.car" text="missing"/>
                        </c:if></h3>
                    </td>
                </tr>
                <tr>
                    <td><spring:message code="mark" text="missing"/>:</td>
                    <td><form:input path="mark"/></td>
                    <td><form:errors path="mark" cssClass="error"/></td>
                </tr>
                <tr>
                    <td><spring:message code="model" text="missing"/>:</td>
                    <td><form:input path="model"/></td>
                    <td><form:errors path="model" cssClass="error"/></td>
                </tr>
                <tr>
                    <td><spring:message code="year" text="missing"/>:</td>
                    <td><form:input path="year"/></td>
                    <td><form:errors path="year" cssClass="error"/></td>
                </tr>
                <tr>
                    <td><spring:message code="color" text="missing"/>:</td>
                    <td><form:input path="color"/></td>
                    <td><form:errors path="color" cssClass="error"/></td>
                </tr>
                <tr>
                    <td><spring:message code="engine.type" text="missing"/>:</td>
                    <td>
                        <form:select path="engine.id">
                            <c:forEach var="engine" items="${engineList}">
                                <form:option value="${engine.id}"><spring:message code="${engine.engineType}"
                                                                                  text="missing"/></form:option>
                            </c:forEach>
                        </form:select></td>
                </tr>
                <tr>
                    <td><spring:message code="class" text="missing"/>:</td>
                    <td><form:select path="carClass.id">
                        <c:forEach var="carClass" items="${carClassList}">
                            <form:option value="${carClass.id}"><spring:message code="${carClass.classType}"
                                                                                text="missing"/></form:option>
                        </c:forEach>
                    </form:select></td>
                    </td>
                </tr>
                <tr>
                    <td><spring:message code="activity.status" text="missing"/>:</td>
                    <td><form:select path="activeStatus">
                        <form:option value="ACTIVE"><spring:message code="ACTIVE" text="missing"/></form:option>
                        <form:option value="INACTIVE"><spring:message code="INACTIVE" text="missing"/></form:option>
                    </form:select></td>
                    <td><form:errors path="activeStatus"/></td>
                    </td>
                </tr>
                <tr>
                    <td><spring:message code="cost.per.day" text="missing"/></td>
                    <td><form:input path="costPerDay" size="4"/></td>
                </tr>
                <tr>
                    <td><spring:message code="image"/></td>
                    <td><input name="image" type="file" onchange="checkFileSize(this)"></td>
                    <c:if test="${uploadError != null}">
                        <td class="error"><spring:message code="file.not.jpeg.or.png" text="missing"/></td>
                    </c:if>
                </tr>
                <tr>
                    <td></td>
                    <td><input class="btn btn-success btn-sm" type="submit"
                               value="<spring:message code="save" text="missing"/>"></td>
                </tr>
            </table>
            </form:form>
        </article>
    </div>

    <%--row4--%>
    <jsp:include page="/WEB-INF/pages/fragments/footer.jsp"/>

</div>

</body>
</html>
