<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><spring:message code="detailed.info.about.the.car.number" text="missing"/> ${car.id}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="${pageContext.request.contextPath}/static/css/bootstrap.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/static/css/custom.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/static/img/fav.png" rel="shortcut icon"/>
    <script src="${pageContext.request.contextPath}/static/js/jquery-2.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/deleteConfirm.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/respond.js"></script>
    <script src="http://code.jboxcdn.com/0.3.2/jBox.min.js"></script>
    <link href="http://code.jboxcdn.com/0.3.2/jBox.css" rel="stylesheet">
</head>
<body>
<div class="container">

    <!--row 1-->
    <jsp:include page="/WEB-INF/pages/fragments/header.jsp"/>

    <%--row2--%>
    <jsp:include page="/WEB-INF/pages/fragments/menu.jsp"/>

    <%--row3--%>
    <jsp:include page="/WEB-INF/pages/fragments/signup-login.jsp"/>

    <%--row4--%>
    <div class="row content">
        <aside class="col-xs-2">
            <jsp:include page="/WEB-INF/pages/fragments/carclasses.jsp"/>
        </aside>
        <article class="col-xs-10">
            <table>
                <a href="${pageContext.request.contextPath}/car/image/${car.id}" title="${car.mark} ${car.model}" data-jbox-image="gallery1"><img width="450px" height="300px" src="${pageContext.request.contextPath}/car/image/${car.id}" alt=""></a>

                <tr>
                    <td><spring:message code="mark" text="missing"/></td>
                    <td>${car.mark}</td>
                </tr>
                <tr>
                    <td><spring:message code="model" text="missing"/></td>
                    <td>${car.model}</td>
                </tr>
                <tr>
                    <td><spring:message code="year" text="missing"/></td>
                    <td>${car.year}</td>
                </tr>
                <tr>
                    <td><spring:message code="class" text="missing"/></td>
                    <td><spring:message code="${car.carClass.classType}" text="missing"/></td>
                </tr>
                <tr>
                    <td><spring:message code="color" text="missing"/></td>
                    <td>${car.color}</td>
                </tr>
                <tr>
                    <td><spring:message code="engine.type" text="missing"/></td>
                    <td><spring:message code="${car.engine.engineType}" text="missing"/></td>
                </tr>
                <tr>
                    <td><spring:message code="cost.per.day" text="missing"/></td>
                    <td><fmt:formatNumber type="number" value="${car.costPerDay}"></fmt:formatNumber></td>
                </tr>
                <tr>
                    <td><spring:message code="activity.status" text="missing"/></td>
                    <td>${car.activeStatus}</td>
                </tr>
            </table>
            <sec:authorize access="hasAuthority('ADMIN')">
                <jsp:include page="/WEB-INF/pages/fragments/deleteConfirm.jsp"/>
                <p><a class="btn btn-warning btn-xs"
                      href="${pageContext.request.contextPath}/car/update/${car.id}"><spring:message code="edit"
                                                                                                     text="missing"/></a>

                    <a class="btn btn-danger btn-xs"
                       data-confirm="<spring:message code="are.you.sure.to.delete.this?"/>"
                       href="${pageContext.request.contextPath}/car/delete/${car.id}"><spring:message code="delete"
                                                                                                      text="missing"/></a>
                </p>
            </sec:authorize>
            <sec:authorize access="isAuthenticated()">
                <c:choose>
                    <c:when test="${car.activeStatus=='INACTIVE'}">
                        <p><spring:message code="sorry.the.car.is.na" text="missing"/></p>
                    </c:when>
                    <c:otherwise>
                        <p><a class="btn btn-primary btn"
                              href="${pageContext.request.contextPath}/order/create/?car=${car.id}&user=<sec:authentication property="principal.id"/>"><spring:message
                                code="rent"
                                text="missing"/></a></p>
                    </c:otherwise>
                </c:choose>
            </sec:authorize>
        </article>
    </div>
    <%--row5--%>
    <jsp:include page="/WEB-INF/pages/fragments/footer.jsp"/>

</div>

<script>

    new jBox('Image');

</script>
</body>
</html>
