<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <title><spring:message code="car.list"/></title>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="${pageContext.request.contextPath}/static/css/bootstrap.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/static/css/custom.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/static/img/fav.png" rel="shortcut icon"/>
    <script src="${pageContext.request.contextPath}/static/js/jquery-2.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/respond.js"></script>

</head>
<body>
<div class="container">
    <%--row1--%>
    <jsp:include page="/WEB-INF/pages/fragments/header.jsp"/>

    <%--row2--%>
    <jsp:include page="/WEB-INF/pages/fragments/menu.jsp"/>

    <%--row3--%>
    <jsp:include page="/WEB-INF/pages/fragments/signup-login.jsp"/>

    <%--row4--%>
    <div class="row content">
        <aside class="col-xs-2">
            <jsp:include page="/WEB-INF/pages/fragments/carclasses.jsp"/>
        </aside>
        <article class="col-xs-10">
            <table id="carTable" class="fashion-table">
                <caption><h2><spring:message code="car.list"/></h2></caption>
                <thead>
                <tr>
                    <th></th>
                    <th><spring:message code="mark" text="missing"/></th>
                    <th><spring:message code="model" text="missing"/></th>
                    <th><spring:message code="year" text="missing"/></th>
                    <th><spring:message code="class" text="missing"/></th>
                    <th><spring:message code="color" text="missing"/></th>
                    <th><spring:message code="engine.type" text="missing"/></th>
                    <th><spring:message code="activity.status" text="missing"/></th>
                    <th><spring:message code="cost" text="missing"/></th>
                    <sec:authorize access="isAuthenticated()">
                        <th><spring:message code="action" text="missing"/></th>
                    </sec:authorize>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="car" items="${page.content}">
                    <tr>
                        <td><a href="${pageContext.request.contextPath}/car/carInfo/${car.id}"><img
                                src="${pageContext.request.contextPath}/car/image/${car.id}" alt="image" height="80px"
                                width="115px"></a></td>
                        <td><a href="${pageContext.request.contextPath}/car/carInfo/${car.id}">${car.mark}</a></td>
                        <td>${car.model}</td>
                        <td>${car.year}</td>
                        <td><spring:message code="${car.carClass.classType}" text="missing"/></td>
                        <td>${car.color}</td>
                        <td><spring:message code="${car.engine.engineType}" text="missing"/></td>
                        <td><spring:message code="${car.activeStatus}" text="missing"/></td>
                        <td><fmt:formatNumber type="number" value="${car.costPerDay}"/></td>
                        <sec:authorize access="isAuthenticated()">
                            <c:choose>
                                <c:when test="${car.activeStatus=='INACTIVE'}">
                                    <td><a onClick="return false;" class="disabled btn btn-primary btn-sm"
                                           href="${pageContext.request.contextPath}/order/create/?car=${car.id}&user=<sec:authentication property="principal.id"/>"><spring:message
                                            code="rent" text="missing"/></a></td>
                                </c:when>
                                <c:otherwise>
                                    <td>
                                        <a class=" btn btn-primary btn-sm"
                                           href="${pageContext.request.contextPath}/order/create/?car=${car.id}&user=<sec:authentication property="principal.id"/>"><spring:message
                                                code="rent" text="missing"/></a></td>
                                </c:otherwise>
                            </c:choose>
                        </sec:authorize>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </article>
    </div>

    <%--row5--%>
    <c:if test="${page.totalPages > 1}">
        <div class="row">
            <div class="col-xs-12" style="text-align: center;">
                <ul class="pagination">
                    <c:forEach begin="1" var="number" end="${page.totalPages}">
                        <li><a href="${pageContext.request.contextPath}/car/list/${number}">${number}</a></li>
                    </c:forEach>
                </ul>
            </div>
        </div>
    </c:if>

    <%--row6--%>
    <jsp:include page="/WEB-INF/pages/fragments/footer.jsp"/>
</div>


</body>
</html>
