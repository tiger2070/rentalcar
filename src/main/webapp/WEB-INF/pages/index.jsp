<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <title>RentalCAR - <spring:message code="rent.a.car"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="${pageContext.request.contextPath}/static/css/bootstrap.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/static/css/custom.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/static/img/fav.png" rel="shortcut icon"/>
    <script src="${pageContext.request.contextPath}/static/js/jquery-2.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/respond.js"></script>
</head>
<body>
<div class="container">
    <!--row 1-->
    <jsp:include page="/WEB-INF/pages/fragments/header.jsp"/>

    <!--row 2-->
    <jsp:include page="/WEB-INF/pages/fragments/menu.jsp"/>

    <!--row 3-->
    <div class="jumbotron">
        <div class="col-xs-4"><img src="${pageContext.request.contextPath}/static/img/normal_porsche_64.jpg"
                                   class="img-thumbnail"></div>
        <div class="col-xs-4"><img src="${pageContext.request.contextPath}/static/img/normal_porsche_63.jpg"
                                   class="img-thumbnail"></div>
        <div class="col-xs-4"><img src="${pageContext.request.contextPath}/static/img/normal_porsche_62.jpg"
                                   class="img-thumbnail"></div>
        <div class="row">
            <%--row 4--%>
            <p style="text-align: center"><a class="btn btn-success btn-lg"
                                             href="${pageContext.request.contextPath}/car/list"><spring:message
                    code="car.list"/></a></p>
        </div>
    </div>


    <!--row 6-->
    <div class="content row">
        <aside class="col-xs-2">
            <jsp:include page="/WEB-INF/pages/fragments/carclasses.jsp"/>
        </aside>

        <article class="col-xs-5 col-xs-offset-2">
            <p><h4><spring:message code="min.term.one.day"/></h4></p>
        </article>
        <article class="col-xs-3">
            <p><spring:message code="news.of.company" text="missing"/>:</p>

            <p>11.09.2014 - <spring:message code="now.we.have" text="missing"/> <a
                    href="${pageContext.request.contextPath}/car/carInfo/12">BMW i8</a></p>

            <p>10.09.2014 - <spring:message code="now.we.have" text="missing"/> <a
                    href="${pageContext.request.contextPath}/car/carInfo/5">Lamborghini
                Aventador</a></p>

            <p>07.09.2014 - <spring:message code="now.we.have" text="missing"/> <a
                    href="${pageContext.request.contextPath}/car/carInfo/10">Bugatti Veyron</a></p>

            <p>29.08.2014 - <spring:message code="now.we.have" text="missing"/> <a
                    href="${pageContext.request.contextPath}/car/carInfo/13">Ferrari F12
                Berlinetta</a></p>

        </article>
    </div>

    <%--row7--%>
    <jsp:include page="/WEB-INF/pages/fragments/footer.jsp"/>

</div>

</body>
</html>
