<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<header class="row">
    <div class="col-xs-3">
        <a href="${pageContext.request.contextPath}/"><img src="${pageContext.request.contextPath}/static/img/logo.png"
                                                           class="img-responsive img-rounded"></a>
    </div>
    <div class="col-xs-6">
        <p>

        <h3><spring:message code="rent.a.car" text="missing"/></h3></p>
    </div>
    <div class="col-xs-2">
        <sec:authorize access="isAuthenticated()">
            <p><spring:message code="welcome" text="missing"></spring:message>, <sec:authentication
                    property="principal.login" scope="session"/></p>
        </sec:authorize>
    </div>
    <div class="col-xs-1">
        <c:set var="url" value="${requestScope['javax.servlet.forward.request_uri']}"/>
        <c:set var="urlRu">
            <c:choose>
                <c:when test="${fn:contains(url, '/order/create')}">${pageContext.request.contextPath}/order/create/?car=${entity.car.id}&user=${entity.user.id}&lang=ru</c:when>
                <c:otherwise>?lang=ru</c:otherwise>
            </c:choose>
        </c:set>
        <c:set var="urlEn">
            <c:choose>
                <c:when test="${fn:contains(url, '/order/create')}">${pageContext.request.contextPath}/order/create/?car=${entity.car.id}&user=${entity.user.id}&lang=en</c:when>
                <c:otherwise>?lang=en</c:otherwise>
            </c:choose>
        </c:set>
        <a title="Russian" href="${urlRu}"><img
                src="${pageContext.request.contextPath}/static/img/RU.png"></a><br/>

        <a title="English" href="${urlEn}"><img
                src="${pageContext.request.contextPath}/static/img/GB.png"></a><br/>

    </div>
</header>