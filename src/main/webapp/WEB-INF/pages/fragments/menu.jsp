<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="row">
    <div class="col-xs-12">
        <ul class="nav nav-justified">
            <li><a href="${pageContext.request.contextPath}/car/list"><spring:message
                    code="car.list"/></a></li>
            <sec:authorize access="isAnonymous()">
                <li><a href="${pageContext.request.contextPath}/user/create"><spring:message
                        code="register"/></a></li>
                <li><a href="${pageContext.request.contextPath}/user/login"><spring:message
                        code="log.in"/></a></li>
            </sec:authorize>
            <sec:authorize access="hasAuthority('ADMIN')">
                <li><a href="${pageContext.request.contextPath}/car/create"><spring:message
                        code="add.car"/></a></li>
                <li><a href="${pageContext.request.contextPath}/order/list"><spring:message
                        code="order.list"/></a></li>
                <li><a href="${pageContext.request.contextPath}/order/bids"><spring:message
                        code="bid.list"/></a></li>
                <li><a href="${pageContext.request.contextPath}/user/list"><spring:message
                        code="list.user"/></a></li>
            </sec:authorize>
            <sec:authorize access="isAuthenticated()">
                <li>
                    <a href="${pageContext.request.contextPath}/user/update/<sec:authentication property="principal.id"/>"><spring:message
                            code="edit.details"/></a></li>
            </sec:authorize>
            <sec:authorize access="hasAuthority('CLIENT')">
                <li>
                    <a href="${pageContext.request.contextPath}/order/orders/<sec:authentication property="principal.id"/>"><spring:message
                            code="my.orders" text="missing"/></a></li>
            </sec:authorize>
            <sec:authorize access="isAuthenticated()">
                <li><a href="${pageContext.request.contextPath}/logout"
                       onclick="document.getElementById('logout-form').submit(); return false;"><spring:message
                        code="logout"/></a></li>
            </sec:authorize>

        </ul>
        <form:form id="logout-form" method="post" action="${pageContext.request.contextPath}/logout">
        </form:form>
    </div>
</div>
