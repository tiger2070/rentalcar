<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="row">
    <div class="col-xs-12" style="text-align: center">
        <sec:authorize access="isAnonymous()">
            <p><spring:message code="to.make.a.order" text="missing"/> <a
                    href="${pageContext.request.contextPath}/user/login"><spring:message code="log.in"
                                                                                         text="missing"/></a>
                <spring:message code="or" text="missing"/> <a
                        href="${pageContext.request.contextPath}/user/create"><spring:message code="register"
                                                                                              text="missing"/></a></p>
        </sec:authorize>
    </div>
</div>
