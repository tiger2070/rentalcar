<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<ul class="nav nav-pills nav-stacked">
    <li><a href="${pageContext.request.contextPath}/car/byClass/1?page=1"><spring:message code="business"
                                                                                          text="missing"/></a>
    </li>
    <li><a href="${pageContext.request.contextPath}/car/byClass/2?page=1"><spring:message code="family"
                                                                                          text="missing"/></a>
    </li>
    <li><a href="${pageContext.request.contextPath}/car/byClass/3?page=1"><spring:message code="medium"
                                                                                          text="missing"/></a>
    </li>
    <li><a href="${pageContext.request.contextPath}/car/byClass/4?page=1"><spring:message code="small"
                                                                                          text="missing"/></a>
    </li>
    <li><a href="${pageContext.request.contextPath}/car/byClass/5?page=1"><spring:message code="suv"
                                                                                          text="missing"/></a></li>
    <li><a href="${pageContext.request.contextPath}/car/byClass/6?page=1"><spring:message code="sport"
                                                                                          text="missing"/></a>
    </li>
</ul>



