<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <title><spring:message code="order.list" text="missing"/></title>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <link href="${pageContext.request.contextPath}/static/css/bootstrap.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/static/css/custom.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/static/img/fav.png" rel="shortcut icon"/>
    <script src="${pageContext.request.contextPath}/static/js/jquery-2.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/deleteConfirm.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/respond.js"></script>
</head>
<body>
<div class="container">
    <!--row 1-->
    <jsp:include page="/WEB-INF/pages/fragments/header.jsp"/>

    <!--row 2-->
    <jsp:include page="/WEB-INF/pages/fragments/menu.jsp"/>

    <%--row3--%>
    <div class="content row">
        <aside class="col-xs-2">
            <jsp:include page="/WEB-INF/pages/fragments/carclasses.jsp"/>
        </aside>

        <article class="col-xs-10">
            ${message}
            <table class="fashion-table">
                <caption><h2><spring:message code="order.list" text="missing"/></h2></caption>
                <thead>
                <tr>
                    <th>№</th>
                    <th><spring:message code="user" text="missing"/></th>
                    <th><spring:message code="car" text="missing"/></th>
                    <th><spring:message code="date.of.order" text="missing"/></th>
                    <th><spring:message code="date.of.rent" text="missing"/></th>
                    <th><spring:message code="date.of.return" text="missing"/></th>
                    <th><spring:message code="status.order" text="missing"/></th>
                    <th><spring:message code="cost" text="missing"/></th>
                    <sec:authorize access="hasAuthority('ADMIN')">
                        <th><spring:message code="action" text="missing"/></th>
                    </sec:authorize>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="order" items="${page.content}">
                    <tr>
                        <td>${order.id}</td>
                        <td>${order.user.login}</td>
                        <td>
                            <a href="${pageContext.request.contextPath}/car/carInfo/${order.car.id}">${order.car.mark} ${order.car.model}</a>
                        </td>
                        <td><fmt:formatDate value="${order.dateOrder}" pattern="HH:mm:ss dd.MM.yy"/></td>
                        <td><fmt:formatDate value="${order.dateIn}" pattern="dd.MM.yyyy"/></td>
                        <td><fmt:formatDate value="${order.dateOut}" pattern="dd.MM.yyyy"/></td>
                        <td><spring:message code="${order.orderStatus}" text="missing"/></td>
                        <td><fmt:formatNumber type="number" value="${order.totalCost}"/></td>
                        <sec:authorize access="hasAuthority('ADMIN')">
                            <td><a class="bg-warning"
                                   href="${pageContext.request.contextPath}/order/update/${order.id}"><spring:message
                                    code="edit"
                                    text="missing"/></a>
                                <jsp:include page="/WEB-INF/pages/fragments/deleteConfirm.jsp"/>
                                <a class="bg-danger"
                                   data-confirm="<spring:message code="are.you.sure.to.delete.this?"/>"
                                   href="${pageContext.request.contextPath}/order/delete/${order.id}"><spring:message
                                        code="delete"
                                        text="missing"/></a>
                            </td>
                        </sec:authorize>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </article>
    </div>

    <%--row4--%>
    <c:if test="${page.totalPages > 1}">
        <div class="row">
            <div class="col-xs-12" style="text-align: center;">
                <ul class="pagination">
                    <c:forEach begin="1" var="number" end="${page.totalPages}">
                        <li><a href="${pageContext.request.contextPath}/order/list/${number}">${number}</a></li>
                    </c:forEach>
                </ul>
            </div>
        </div>
    </c:if>

    <%--row5--%>
    <jsp:include page="/WEB-INF/pages/fragments/footer.jsp"/>

</div>

</body>
</html>
