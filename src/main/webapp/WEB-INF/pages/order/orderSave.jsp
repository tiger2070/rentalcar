<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><spring:message code="details.for.order" text="missing"/></title>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <link href="${pageContext.request.contextPath}/static/css/bootstrap.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/static/css/custom.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/static/img/fav.png" rel="shortcut icon"/>
    <script src="${pageContext.request.contextPath}/static/js/jquery-2.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/jquery-ui.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/respond.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/countDays.js"></script>
</head>
<body>
<div class="container">
    <!--row 1-->
    <jsp:include page="/WEB-INF/pages/fragments/header.jsp"/>

    <!--row 2-->
    <jsp:include page="/WEB-INF/pages/fragments/menu.jsp"/>

    <%--row3--%>
    <div class="content row">
        <aside class="col-xs-2">
            <jsp:include page="/WEB-INF/pages/fragments/carclasses.jsp"/>
        </aside>

        <article class="col-xs-10">
            <table class="fashion-table">
                <c:set var="save">
                    <c:if test="${entity.id==null}">${pageContext.request.contextPath}/order/create</c:if>
                    <c:if test="${entity.id!=null}">${pageContext.request.contextPath}/order/update/${entity.id}</c:if>
                </c:set>
                <form:form method="post" commandName="entity" action="${save}" enctype="multipart/form-data">
                <form:hidden path="id"/>
                <form:hidden path="version"/>
                <form:hidden path="car.id"/>
                <form:hidden path="user.id"/>
                <h1><spring:message code="details.for.order" text="missing"/></h1>

                <tr>
                    <th></th>
                    <th><spring:message code="car" text="missing"/></th>
                </tr>
                <tr>
                    <td><spring:message code="mark" text="missing"/></td>
                    <td>${entity.car.mark}</td>
                </tr>
                <tr>
                    <td><spring:message code="model" text="missing"/></td>
                    <td>${entity.car.model}</td>
                </tr>
                <tr>
                    <td><spring:message code="color" text="missing"/></td>
                    <td>${entity.car.color}</td>
                </tr>
                <tr>
                    <td><spring:message code="year" text="missing"/></td>
                    <td>${entity.car.year}</td>
                </tr>
                <tr>
                    <td><spring:message code="engine.type" text="missing"/></td>
                    <td><spring:message code="${entity.car.engine.engineType}" text="missing"/></td>
                </tr>
                <tr>
                    <td><spring:message code="class" text="missing"/></td>
                    <td><spring:message code="${entity.car.carClass.classType}" text="missing"/></td>
                </tr>
                <tr>
                    <td><spring:message code="cost.per.day" text="missing"/></td>
                    <td><fmt:formatNumber type="number" value="${entity.car.costPerDay}"/></td>
                    <input type="hidden" value="${entity.car.costPerDay}" id="costPerDay"/>
                </tr>
                <tr>
                    <th></th>
                    <th><spring:message code="passport.details" text="missing"/></th>
                </tr>
                <tr>
                    <td><spring:message code="lastname" text="missing"/></td>
                    <td>${entity.user.lastName}</td>
                </tr>
                <tr>
                    <td><spring:message code="firstname" text="missing"/></td>
                    <td>${entity.user.firstName}</td>
                </tr>
                <tr>
                    <td><spring:message code="passport.number" text="missing"/></td>
                    <td>${entity.user.passport.seriesAndNumber}</td>
                </tr>
                <tr>
                    <td><spring:message code="place.of.issue" text="missing"/></td>
                    <td>${entity.user.passport.issuedBy}</td>
                </tr>
                <tr>
                    <td><spring:message code="date.of.issue" text="missing"/></td>
                    <td><fmt:formatDate value="${entity.user.passport.givingDate}" pattern="dd.MM.yyyy"/></td>
                </tr>
                <tr>
                    <td><spring:message code="date.of.expiry" text="missing"/></td>
                    <td><fmt:formatDate value="${entity.user.passport.expiryDate}" pattern="dd.MM.yyyy"/></td>
                </tr>
                <tr>
                    <th></th>
                    <th><spring:message code="date" text="missing"/></th>
                </tr>
                <tr>
                    <spring:message code="dd.mm.yyyy" var="datePlaceholder"/>
                    <td><form:label path="dateIn"><spring:message code="date.of.rent" text="missing"/></form:label></td>
                    <td><form:input id="TxtFromDate" path="dateIn" placeholder="${datePlaceholder}"></form:input></td>
                    <td class="error"><form:errors path="dateIn"/>
                        <c:if test="${carIsBusy!=null}">
                            <spring:message code="car.is.busy" text="missing"/>
                        </c:if>
                    </td>
                </tr>
                <tr>
                    <td><form:label path="dateOut"><spring:message code="date.of.return"
                                                                   text="missing"/></form:label></td>
                    <td><form:input id="TxtToDate" path="dateOut" placeholder="${datePlaceholder}"></form:input></td>
                    <td><form:errors path="dateOut" cssClass="error"/></td>
                </tr>
                <tr>
                    <td><spring:message code="cost" text="missing"/></td>
                    <td><input type="text" id="cost" readonly></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <button type="submit" class="btn btn-primary"><spring:message code="save"/></button>
                    </td>
                </tr>
            </table>

            </form:form>
        </article>
    </div>

    <%--row4--%>
    <jsp:include page="/WEB-INF/pages/fragments/footer.jsp"/>
</div>



</body>
</html>
