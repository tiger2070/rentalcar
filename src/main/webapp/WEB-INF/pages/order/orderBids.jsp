<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><spring:message code="unprocessed.bids" text="missing"/></title>
    <meta http-equiv="Content-Type" charset="UTF-8">
    <link href="${pageContext.request.contextPath}/static/css/bootstrap.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/static/css/custom.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/static/img/fav.png" rel="shortcut icon"/>
    <script src="${pageContext.request.contextPath}/static/js/jquery-2.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/respond.js"></script>
</head>
<body>
<div class="container">
    <%--row1--%>
    <jsp:include page="/WEB-INF/pages/fragments/header.jsp"/>

    <!--row 2-->
    <jsp:include page="/WEB-INF/pages/fragments/menu.jsp"/>

    <!--row 3-->
    <div class="content row">
        <aside class="col-xs-2">
            <jsp:include page="/WEB-INF/pages/fragments/carclasses.jsp"/>
        </aside>

        <article class="col-xs-10">

            <c:if test="${empty bids.orders}">
                <h2><spring:message code="there.are.no.new.orders" text="missing"/></h2>
            </c:if>
            <c:if test="${!empty bids.orders}">
                <form:form method="post" commandName="bids" action="${pageContext.request.contextPath}/order/bids">
                    <table class="fashion-table">
                        <caption><h3><spring:message code="unprocessed.bids" text="missing"/></h3></caption>
                        <tr>
                            <th>№</th>
                            <th><spring:message code="user" text="missing"/></th>
                            <th><spring:message code="car" text="missing"/></th>
                            <th><spring:message code="date.of.order" text="missing"/></th>
                            <th><spring:message code="date.of.rent" text="missing"/></th>
                            <th><spring:message code="date.of.return" text="missing"/></th>
                            <th><spring:message code="status.order" text="missing"/></th>
                        </tr>
                        <c:forEach var="order" items="${bids.orders}" varStatus="status">
                            <tr>
                                <td>${order.id}
                                    <form:hidden path="orders[${status.index}].id" value="${order.id}"/>
                                </td>
                                <td>${order.user.login}</td>
                                <td>${order.car.mark} ${order.car.model}</td>
                                <td><fmt:formatDate value="${order.dateOrder}" pattern="HH:mm:ss dd.MM.yy"/></td>
                                <td><fmt:formatDate value="${order.dateIn}" pattern="dd.MM.yyyy"/></td>
                                <td><fmt:formatDate value="${order.dateOut}" pattern="dd.MM.yyyy"/></td>
                                <td><form:select path="orders[${status.index}].orderStatus">
                                    <form:option value="PENDING"><spring:message code="PENDING"
                                                                                 text="missing"/></form:option>
                                    <form:option value="CONFIRMED"><spring:message code="CONFIRMED"
                                                                                   text="missing"/></form:option>
                                    <form:option value="DECLINED"><spring:message code="DECLINED"
                                                                                  text="missing"/></form:option>
                                </form:select>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                    <input class="btn btn-success btn-sm" type="submit"
                           value="<spring:message code="save" text="missing"/>">
                </form:form>
            </c:if>
        </article>
    </div>


    <%--row4--%>
    <div class="row">
        <div class="col-xs-12" style="text-align: center;">
            <ul class="pagination">
                <c:forEach begin="1" var="number" end="${page.totalPages}">
                    <li><a href="${pageContext.request.contextPath}/order/bids?page=${number}">${number}</a></li>
                </c:forEach>
            </ul>
        </div>
    </div>


    <%--row5--%>
    <jsp:include page="/WEB-INF/pages/fragments/footer.jsp"/>

</div>
</body>
</html>
