<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><spring:message code="page.of.changing.order" text="missing"/></title>
    <link href="${pageContext.request.contextPath}/static/css/bootstrap.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/static/css/custom.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/static/img/fav.png" rel="shortcut icon"/>
    <script src="${pageContext.request.contextPath}/static/js/jquery-2.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/respond.js"></script>
</head>
<body>
<div class="container">
    <%--row1--%>
    <jsp:include page="/WEB-INF/pages/fragments/header.jsp"/>
    <%--row2--%>
    <jsp:include page="/WEB-INF/pages/fragments/menu.jsp"/>
    <%--row3--%>
    <div class="content row">
        <aside class="col-xs-2">
            <jsp:include page="/WEB-INF/pages/fragments/carclasses.jsp"/>
        </aside>

        <article class="col-xs-10">
            <p>

            <h3><c:if test="${messageCreate != null}">
                <spring:message code="order" text="missing"/> <spring:message code="number"
                                                                              text="missing"/> ${messageCreate}
                <spring:message code="success.created" text="missing"/>
            </c:if>
                <c:if test="${messageUpdate != null}">
                    <spring:message code="order" text="missing"/> <spring:message code="number"
                                                                                  text="missing"/> ${messageUpdate}
                    <spring:message code="success.changed" text="missing"/>
                </c:if>
                <c:if test="${messageDelete != null}">
                    <spring:message code="order" text="missing"/> <spring:message code="number"
                                                                                  text="missing"/> ${messageDelete}
                    <spring:message code="success.removed" text="missing"/>
                </c:if>
                <c:if test="${messageUpdateStatusOrder != null}">
                    <spring:message code="processed" text="missing"/> ${messageUpdateStatusOrder} <spring:message
                        code="orders"
                        text="missing"/>
                </c:if></h3></p>
        </article>
    </div>

    <%--row4--%>
    <jsp:include page="/WEB-INF/pages/fragments/footer.jsp"/>

</div>

</body>
</html>
