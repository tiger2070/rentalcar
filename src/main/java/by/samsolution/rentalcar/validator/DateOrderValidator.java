package by.samsolution.rentalcar.validator;

import by.samsolution.rentalcar.entity.Order;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Component
public class DateOrderValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Order.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Order order = (Order) target;

        if (order.getDateIn() != null && order.getDateOut() != null) {
            if (order.getDateOut().compareTo(order.getDateIn()) < 0) {
                errors.rejectValue("dateOut", "dateOut.less.than.dateIn");
            }
            if (order.getDateOut().compareTo(order.getDateIn()) == 0) {
                errors.rejectValue("dateOut", "min.term.one.day");
            }
            // this trunk only for creating order
            if (order.getId() == null && order.getDateIn().compareTo(getCurrentDateWithoutTime()) < 0) {
                errors.rejectValue("dateIn", "dateIn.less.than.today");
            }
        }
    }

    /**
     * Getting current Date without timestamp
     *
     * @return
     */
    private Date getCurrentDateWithoutTime() {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String curDate = df.format(Calendar.getInstance().getTime());
        Date currentDate = null;
        try {
            currentDate = df.parse(curDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return currentDate;
    }
}
