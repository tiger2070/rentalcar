package by.samsolution.rentalcar.repository;

import by.samsolution.rentalcar.entity.Engine;
import org.springframework.data.jpa.repository.JpaRepository;


public interface EngineRepository extends JpaRepository<Engine, Integer> {

}
