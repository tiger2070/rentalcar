package by.samsolution.rentalcar.repository;

import by.samsolution.rentalcar.entity.Car;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<Car, Integer> {
    //    @Query("Select c from Car c where c.carClass.id =?1 ")
    Page<Car> findCarByCarClassId(Integer id, Pageable p);
}
