package by.samsolution.rentalcar.repository;

import by.samsolution.rentalcar.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User, Integer> {

    @Query(value = "select u from User u where u.login = ?1")
    User getUser(String login);

}
