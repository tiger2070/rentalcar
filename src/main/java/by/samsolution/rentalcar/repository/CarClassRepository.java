package by.samsolution.rentalcar.repository;

import by.samsolution.rentalcar.entity.CarClass;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarClassRepository extends JpaRepository<CarClass, Integer> {

}
