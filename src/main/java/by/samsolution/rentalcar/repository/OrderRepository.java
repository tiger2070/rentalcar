package by.samsolution.rentalcar.repository;

import by.samsolution.rentalcar.entity.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Integer> {

    /**
     * Getting new orders - orders with status "PENDING"
     *
     * @return
     */
    @Query("select o from Order o where o.orderStatus ='PENDING'")
    List<Order> getNewOrders();

    Page<Order> findOrderByUserId(Integer id, Pageable pageable);

    /**
     * Checks whether the car isn't busy for order creating
     *
     * @param carId
     * @param dateIn
     * @param dateOut
     * @return
     */
    @Query(value = "SELECT * FROM rentalcar.`order`where id_car =?1 and order_status != 'DECLINED' and " +
            "((date_in between ?2 and ?3) or (?2 between date_in and date_out))", nativeQuery = true)
    List<Order> checkAvailableCarForOrderCreate(Integer carId, Date dateIn, Date dateOut);

    /**
     * Checks whether the car isn't busy for order updating
     *
     * @param carId
     * @param dateIn
     * @param dateOut
     * @param orderId
     * @return
     */
    @Query(value = "SELECT * FROM rentalcar.`order`where id_car =?1 and id_order !=?4 and order_status != 'DECLINED' and " +
            "((date_in between ?2 and ?3) or (?2 between date_in and date_out))", nativeQuery = true)
    List<Order> checkAvailableCarForOrderUpdate(Integer carId, Date dateIn, Date dateOut, Integer orderId);

}
