package by.samsolution.rentalcar.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.Resource;

@Configuration
@EnableWebMvcSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Resource
    private UserDetailsService userDetailsService;
    @Resource
    private PasswordEncoder bCryptPasswordEncoder;


    /*
    Register my userDetailsService realization and password-encoder
     */
    @Resource
    public void registerGlobalAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService)
                .passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/order/list**/**").hasAuthority("ADMIN")
                .antMatchers("/order/bids**/**").hasAuthority("ADMIN")
                .antMatchers("/order/create/**").authenticated()
                .antMatchers("/order/orders/**").authenticated()
                .antMatchers("/order/delete/**").hasAuthority("ADMIN")
                .antMatchers("/order/update/**").hasAuthority("ADMIN")
                .antMatchers("/user/list**/**").hasAuthority("ADMIN")
                .antMatchers("/user/create/").anonymous()
                .antMatchers("/user/update/**").authenticated()
                .antMatchers("/user/delete/**").hasAuthority("ADMIN")
                .antMatchers("/car/create**/**").hasAuthority("ADMIN")
                .antMatchers("/car/update/**").hasAuthority("ADMIN")
                .antMatchers("/car/delete/**").hasAuthority("ADMIN")
                .anyRequest().permitAll()
                .and();

        http.formLogin()
                // give access all to login-form
                .permitAll()
                .loginPage("/user/login")
                .loginProcessingUrl("/login")
                .defaultSuccessUrl("/user/login-success")
                .failureUrl("/user/login-error")
                .usernameParameter("j_username")
                .passwordParameter("j_password");

        http.logout()
                // allow do logout to all
                .permitAll()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/")
                .invalidateHttpSession(true);

        http.sessionManagement()
                .invalidSessionUrl("/");
    }

    //-- Password encoding --//
    @Bean
    public PasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
