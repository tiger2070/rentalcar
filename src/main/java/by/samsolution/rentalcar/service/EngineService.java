package by.samsolution.rentalcar.service;

import by.samsolution.rentalcar.entity.Engine;

import java.util.List;


public interface EngineService {

    List<Engine> getAllEngines();
}
