package by.samsolution.rentalcar.service;

import by.samsolution.rentalcar.entity.Order;
import org.springframework.data.domain.Page;

import java.util.List;

public interface OrderService extends CrudService<Order> {
    /**
     * Getting bids - orders with status Pending
     *
     * @return
     */
    List<Order> getNewOrders();

    /**
     * Updates statuses of new orders after reviewing by administrator
     *
     * @param orders
     * @return
     */
    List<Order> updateOrderStatus(List<Order> orders);

    Page<Order> getOrderByUserId(Integer id, Integer page);

}
