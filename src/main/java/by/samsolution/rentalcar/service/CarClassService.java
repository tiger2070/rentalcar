package by.samsolution.rentalcar.service;

import by.samsolution.rentalcar.entity.CarClass;

import java.util.List;

public interface CarClassService {
    List<CarClass> getAllCarClasses();
}
