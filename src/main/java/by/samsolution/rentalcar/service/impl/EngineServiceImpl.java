package by.samsolution.rentalcar.service.impl;

import by.samsolution.rentalcar.entity.Engine;
import by.samsolution.rentalcar.repository.EngineRepository;
import by.samsolution.rentalcar.service.EngineService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class EngineServiceImpl implements EngineService {
    @Resource
    EngineRepository engineRepository;


    @Override
    @Transactional
    public List<Engine> getAllEngines() {
        List<Engine> engineList = engineRepository.findAll();
        return engineList;
    }
}
