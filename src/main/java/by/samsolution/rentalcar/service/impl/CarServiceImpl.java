package by.samsolution.rentalcar.service.impl;

import by.samsolution.rentalcar.entity.Car;
import by.samsolution.rentalcar.repository.CarRepository;
import by.samsolution.rentalcar.service.CarService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
public class CarServiceImpl implements CarService {

    @Resource
    private CarRepository carRepository;

    @Override
    @Transactional
    public Car create(Car car) {
        return carRepository.save(car);
    }

    @Override
    @Transactional
    public Car delete(int id) {
        Car deletedCar = carRepository.findOne(id);
        carRepository.delete(deletedCar);
        return deletedCar;
    }

    @Override
    @Transactional
    public Car update(Car car) {
        if (car.getImagePath() == null) {
            Car beforeUpdateCar = findById(car.getId());
            car.setImagePath(beforeUpdateCar.getImagePath());
        }
        return carRepository.save(car);
    }

    @Override
    @Transactional
    public Page<Car> findAll(Integer page) {
        return carRepository.findAll(new PageRequest(page, 10));
    }


    @Override
    @Transactional
    public Car findById(int id) {
        return carRepository.findOne(id);
    }

    @Override
    public Page<Car> getCarsByClass(Integer idClass, Integer page) {
        return carRepository.findCarByCarClassId(idClass, new PageRequest(page, 10, Sort.Direction.DESC, "costPerDay"));
    }
}
