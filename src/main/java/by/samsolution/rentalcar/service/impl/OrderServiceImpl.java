package by.samsolution.rentalcar.service.impl;


import by.samsolution.rentalcar.entity.Order;
import by.samsolution.rentalcar.entity.enumeration.OrderStatus;
import by.samsolution.rentalcar.exception.CarIsBusyException;
import by.samsolution.rentalcar.repository.CarRepository;
import by.samsolution.rentalcar.repository.OrderRepository;
import by.samsolution.rentalcar.service.OrderService;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Resource(name = "orderRepository")
    OrderRepository orderRepository;
    @Resource
    CarRepository carRepository;


    @Override
    @Transactional
    public Order create(Order order) {

        if (isCarAvailable(order)) {
            order.setOrderStatus(OrderStatus.PENDING);
            order.setDateOrder(Calendar.getInstance().getTime());
            order.setTotalCost(countTotalCost(order));
            return orderRepository.save(order);
        } else {
            throw new CarIsBusyException();
        }

    }

    @Override
    @Transactional
    public Order delete(int id) {
        Order orderDeleted = orderRepository.findOne(id);
        orderRepository.delete(orderDeleted);
        return orderDeleted;
    }

    @Override
    @Transactional
    public Order update(Order order) {
        Order beforeUpdateOrder = orderRepository.findOne(order.getId());
        // Merging fresh order and order in db - copy old properties except new
        String[] ignoreProps = {"dateIn", "dateOut", "totalCost", "version"};
        BeanUtils.copyProperties(beforeUpdateOrder, order, ignoreProps);
        if (isCarAvailable(order)) {
            order.setTotalCost(countTotalCost(order));
            return orderRepository.save(order);
        } else {
            throw new CarIsBusyException();
        }
    }

    @Override
    @Transactional
    public Page<Order> findAll(Integer page) {
        return orderRepository.findAll(new PageRequest(page, 20));
    }

    @Override
    @Transactional
    public Order findById(int id) {
        return orderRepository.findOne(id);
    }


    @Override
    @Transactional
    public List<Order> getNewOrders() {
        return orderRepository.getNewOrders();
    }

    @Override
    @Transactional
    public List<Order> updateOrderStatus(List<Order> orders) {
        List<Order> afterUpdateList = new ArrayList<>();
        for (Order order : orders) {
            Order beforeUpdateOrder = orderRepository.findOne(order.getId());
            if (!beforeUpdateOrder.getOrderStatus().equals(order.getOrderStatus())) {
                beforeUpdateOrder.setOrderStatus(order.getOrderStatus());
                Order afterUpdateOrder = orderRepository.save(beforeUpdateOrder);
                afterUpdateList.add(afterUpdateOrder);
            }
        }
        return afterUpdateList;
    }

    @Override
    @Transactional
    public Page<Order> getOrderByUserId(Integer id, Integer page) {
        return orderRepository.findOrderByUserId(id, new PageRequest(page, 10));
    }

    private boolean isCarAvailable(Order order) {
        boolean isAvailable = false;
        List<Order> orders;
        if (order.getId() == null) {
            orders = orderRepository.checkAvailableCarForOrderCreate(order.getCar().getId(), order.getDateIn(), order.getDateOut());
            if (orders.isEmpty()) {
                isAvailable = true;
            }
        } else {
            orders = orderRepository.checkAvailableCarForOrderUpdate(order.getCar().getId(), order.getDateIn(), order.getDateOut(), order.getId());
            if (orders.isEmpty()) {
                isAvailable = true;
            }
        }
        return isAvailable;
    }

    private Double countTotalCost(Order order) {
        Date dateIn = order.getDateIn();
        Date dateOut = order.getDateOut();
        Days d = Days.daysBetween(new LocalDate(dateIn), new LocalDate(dateOut));
        int days = d.getDays();
        Double totalCost = order.getCar().getCostPerDay() * days;
        return totalCost;

    }

}
