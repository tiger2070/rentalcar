package by.samsolution.rentalcar.service.impl;


import by.samsolution.rentalcar.entity.User;
import by.samsolution.rentalcar.entity.UserDetailsCustom;
import by.samsolution.rentalcar.repository.UserRepository;
import org.apache.log4j.Logger;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Transactional
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    Logger LOG = Logger.getLogger(UserDetailsServiceImpl.class);

    @Resource
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = userRepository.getUser(login);
        if (user == null) {
            LOG.error("User " + login + " not found");
            throw new UsernameNotFoundException("User " + login + " not found");
        }
        UserDetails userDetails = new UserDetailsCustom(user.getId(), user.getLogin(), user.getPassword(), user.getAccessLevel());
        return userDetails;
    }
}