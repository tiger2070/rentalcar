package by.samsolution.rentalcar.service.impl;

import by.samsolution.rentalcar.entity.User;
import by.samsolution.rentalcar.entity.enumeration.AccessLevel;
import by.samsolution.rentalcar.exception.LoginAlreadyExistException;
import by.samsolution.rentalcar.repository.UserRepository;
import by.samsolution.rentalcar.service.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserRepository userRepository;
    @Resource
    PasswordEncoder bCryptPasswordEncoder;

    @Override
    @Transactional
    public User create(User user) {
        if (userRepository.getUser(user.getLogin()) != null) {
            throw new LoginAlreadyExistException();
        }
        String encodePassword = bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(encodePassword);
        user.setAccessLevel(AccessLevel.CLIENT);
        User newUser = userRepository.save(user);
        return newUser;
    }

    @Override
    @Transactional
    public User delete(int id) {
        User deletedUser = userRepository.findOne(id);
        userRepository.delete(deletedUser);
        return deletedUser;
    }

    @Override
    @Transactional
    public User update(User user) {
        String encodePassword = bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(encodePassword);
        User updatedUser = userRepository.save(user);
        return updatedUser;
    }

    @Override
    @Transactional
    public Page<User> findAll(Integer page) {
        return userRepository.findAll(new PageRequest(page, 10));
    }

    @Override
    @Transactional
    public User findById(int id) {
        User user = userRepository.findOne(id);
        return user;
    }


    @Override
    @Transactional
    public User getUser(String login) {
        User user = userRepository.getUser(login);
        return user;
    }
}
