package by.samsolution.rentalcar.service.impl;

import by.samsolution.rentalcar.entity.CarClass;
import by.samsolution.rentalcar.repository.CarClassRepository;
import by.samsolution.rentalcar.service.CarClassService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CarClassServiceImpl implements CarClassService {

    @Resource
    private CarClassRepository carClassRepository;

    @Override
    @Transactional
    public List<CarClass> getAllCarClasses() {
        List<CarClass> carClassList = carClassRepository.findAll();
        return carClassList;
    }
}
