package by.samsolution.rentalcar.service;


import by.samsolution.rentalcar.entity.User;

public interface UserService extends CrudService<User> {
    User getUser(String login);
}
