package by.samsolution.rentalcar.service;

import by.samsolution.rentalcar.entity.Car;
import org.springframework.data.domain.Page;

public interface CarService extends CrudService<Car> {
    Page<Car> getCarsByClass(Integer idClass, Integer page);

}
