package by.samsolution.rentalcar.service;

import by.samsolution.rentalcar.entity.IEntity;
import org.springframework.data.domain.Page;

public interface CrudService<T extends IEntity> {

    public T create(T entity);

    public T delete(int id);

    public T update(T entity);

    public Page<T> findAll(Integer page);

    public T findById(int id);


}