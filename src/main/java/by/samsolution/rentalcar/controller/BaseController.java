package by.samsolution.rentalcar.controller;


import by.samsolution.rentalcar.entity.IEntity;
import by.samsolution.rentalcar.exception.BusinessException;
import by.samsolution.rentalcar.service.CrudService;
import org.apache.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.orm.jpa.JpaOptimisticLockingFailureException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.Map;

public abstract class BaseController<T extends IEntity> {

    public static final Logger LOG = Logger.getLogger(BaseController.class);

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView newEntityPage(@RequestParam Map<String, String> allRequestParams) {
        LOG.info("newEntityPage");
        ModelAndView mav = new ModelAndView();
        mav.setViewName(getName() + "Save");
        mav.addObject("entity", buildNewEntity(allRequestParams));
        addComponentsFromStorage(mav);
        return mav;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ModelAndView createNewEntity(@Valid @ModelAttribute("entity") T entity, BindingResult bindingResult,
                                        @RequestParam(value = "image", required = false) MultipartFile file) {
        LOG.info("createNewEntity: " + entity.toString());
        ModelAndView mav = new ModelAndView();
        validateEntity(entity, bindingResult);
        boolean isFileUploaded = uploadFile(file, entity, mav);
        if (bindingResult.hasErrors() || !isFileUploaded) {
            mav.setViewName(getName() + "Save");
            initAdditionalComponents(entity);
            addComponentsFromStorage(mav);
            return mav;
        }
        try {
            mav.setViewName(getName() + "Changed");
            initAdditionalComponents(entity);
            T createdEntity = getCrudService().create(entity);
            mav.addObject("messageCreate", createdEntity.getId());
            return mav;
        } catch (BusinessException e) {
            LOG.info("can't create " + entity.toString());
            return processException(e, entity);
        }
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView entityListFirstPage() {
        LOG.info("entityListFirstPage: ");
        ModelAndView mav = new ModelAndView();
        mav.setViewName(getName() + "List");
        Page<T> page = getCrudService().findAll(0);
        mav.addObject("page", page);
        return mav;
    }

    @RequestMapping(value = "/list/{pageNumber}", method = RequestMethod.GET)
    public ModelAndView entityListPage(@PathVariable Integer pageNumber) {
        LOG.info("entityListFirstPage: ");
        ModelAndView mav = new ModelAndView();
        if (pageNumber < 0) {
            mav.setViewName("error/error404");
            return mav;
        }
        Page<T> page = getCrudService().findAll(pageNumber - 1);
        if (pageNumber > page.getTotalPages()) {
            mav.setViewName("error/error404");
            return mav;
        }
        mav.setViewName(getName() + "List");
        mav.addObject("page", page);
        return mav;
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
    public ModelAndView updateEntityPage(@PathVariable Integer id) {
        LOG.info("updateEntityPage: id -" + id);
        ModelAndView mav = new ModelAndView();
        mav.setViewName(getName() + "Save");
        T entity = getCrudService().findById(id);
        mav.addObject("entity", entity);
        addComponentsFromStorage(mav);
        return mav;
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
    public ModelAndView updateEntity(@Valid @ModelAttribute("entity") T entity, BindingResult bindingResult, @PathVariable Integer id,
                                     @RequestParam(value = "image", required = false) MultipartFile file) {
        ModelAndView mav = new ModelAndView();
        validateEntity(entity, bindingResult);
        boolean isFileUploaded = uploadFile(file, entity, mav);
        if (bindingResult.hasErrors() || !isFileUploaded) {
            mav.setViewName(getName() + "Save");
            initAdditionalComponents(entity);
            addComponentsFromStorage(mav);
            return mav;
        }
        try {
            getCrudService().update(entity);
            mav.setViewName(getName() + "Changed");
            mav.addObject("messageUpdate", entity.getId());
            LOG.info("updated entity: id -" + id);
            return mav;
        } catch (BusinessException e) {
            LOG.info("can't update " + entity.toString());
            return processException(e, entity);
        }
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public ModelAndView deleteEntity(@PathVariable Integer id) {
        LOG.info("delete: id -" + id);
        ModelAndView mav = new ModelAndView();
        mav.setViewName(getName() + "Changed");
        try {
            getCrudService().delete(id);
            mav.addObject("messageDelete", id);
        } catch (org.springframework.orm.jpa.JpaSystemException e) {
            LOG.error("deleteEntity: id " + id, e);
            mav.addObject("cantDelete", id);
            return mav;
        }
        return mav;
    }

    @ExceptionHandler
    public ModelAndView handleOptimisticExc(JpaOptimisticLockingFailureException e) {
        return new ModelAndView("error/optLockError");
    }

    protected abstract String getName();

    protected abstract CrudService<T> getCrudService();

    /**
     * Build new entity that will added to model
     *
     * @param allRequestParams
     * @return
     */

    protected abstract T buildNewEntity(@RequestParam Map<String, String> allRequestParams);

    /**
     * Initializes additional components for creating a entity
     */
    protected void initAdditionalComponents(T entity) {
    }

    /**
     * Add components from Storage to ModelAndView.
     * Components like list of engines, car classes, etc
     *
     * @param mav
     */
    protected void addComponentsFromStorage(ModelAndView mav) {
    }

    /**
     * Validates data obtained from form
     *
     * @param entity
     * @param result
     */
    protected void validateEntity(T entity, BindingResult result) {
    }

    /**
     * Processing BusinessExceptions
     *
     * @param e
     * @param entity
     * @return
     */
    protected ModelAndView processException(BusinessException e, T entity) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("error/error");
        return mav;
    }

    /**
     * Uploads file on server
     *
     * @param file
     * @param entity
     * @param mav
     * @return true if file uploaded or empty
     */
    protected boolean uploadFile(MultipartFile file, T entity, ModelAndView mav) {
        return true;
    }
}
