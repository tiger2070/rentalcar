package by.samsolution.rentalcar.controller;


import by.samsolution.rentalcar.entity.*;
import by.samsolution.rentalcar.entity.enumeration.AccessLevel;
import by.samsolution.rentalcar.exception.BusinessException;
import by.samsolution.rentalcar.exception.CarIsBusyException;
import by.samsolution.rentalcar.service.CarService;
import by.samsolution.rentalcar.service.OrderService;
import by.samsolution.rentalcar.service.UserService;
import by.samsolution.rentalcar.validator.DateOrderValidator;
import org.apache.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/order")
public class OrderController extends BaseController<Order> {
    private final static Logger LOG = Logger.getLogger(OrderController.class);

    @Resource
    private OrderService orderService;
    @Resource
    private CarService carService;
    @Resource
    private UserService userService;
    @Resource
    private DateOrderValidator dateOrderValidator;

    @RequestMapping(value = "/orders/{id}")
    public ModelAndView getOrdersByUserId(@PathVariable Integer id, @RequestParam Map<String, String> allRequestParams) {
        ModelAndView mav = new ModelAndView();
        UserDetailsCustom principal = (UserDetailsCustom) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!principal.getId().equals(id) && !principal.getAccessLevel().equals(AccessLevel.ADMIN)) {
            LOG.info("Attempt to get access to orders user id:" + id + " from user id:" + principal.getId());
            mav.setViewName("error/error404");
            return mav;
        } else {
            Integer page = Integer.parseInt(allRequestParams.get("page")) - 1;
            mav.setViewName(getName() + "List");
            Page<Order> orders = orderService.getOrderByUserId(id, page);
            mav.addObject("page", orders);
            return mav;
        }
    }

    /**
     * Get bids - new orders with status "pending"
     *
     * @return
     */
    @RequestMapping(value = "/bids", method = RequestMethod.GET)
    public ModelAndView bidsPage(@RequestParam Map<String, String> allRequestParams) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName(getName() + "Bids");
        List<Order> newOrders = orderService.getNewOrders();
        Bids bids = new Bids();
        bids.setOrders(newOrders);
        mav.addObject("bids", bids);
        return mav;
    }

    /**
     * Updates statuses of new orders
     *
     * @param bids
     * @return
     */
    @RequestMapping(value = "/bids", method = RequestMethod.POST)
    public ModelAndView changeBids(@ModelAttribute("bids") Bids bids) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName(getName() + "Changed");
        List<Order> updatedStatusOrders = orderService.updateOrderStatus(bids.getOrders());
        mav.addObject("messageUpdateStatusOrder", updatedStatusOrders.size());
        return mav;
    }

    @Override
    protected void initAdditionalComponents(Order order) {
        Car car = carService.findById(order.getCar().getId());
        User user = userService.findById(order.getUser().getId());
        order.setUser(user);
        order.setCar(car);
    }

    @Override
    public String getName() {
        return "order/order";
    }

    @Override
    protected OrderService getCrudService() {
        return orderService;
    }

    @Override
    protected Order buildNewEntity(@RequestParam Map<String, String> allRequestParams) {
        Integer idCar = Integer.parseInt(allRequestParams.get("car"));
        Integer idUser = Integer.parseInt(allRequestParams.get("user"));
        Car car = carService.findById(idCar);
        User user = userService.findById(idUser);
        Order order = new Order();
        order.setCar(car);
        order.setUser(user);
        return order;
    }

    @Override
    protected void validateEntity(Order order, BindingResult result) {
        dateOrderValidator.validate(order, result);
    }

    @Override
    protected ModelAndView processException(BusinessException e, Order order) {
        ModelAndView mav = new ModelAndView();
        if (e instanceof CarIsBusyException) {
            LOG.info("Car id: " + order.getCar().getId() + " is busy");
            mav.setViewName(getName() + "Save");
            initAdditionalComponents(order);
            mav.addObject("carIsBusy", "car.is.busy");
        }
        return mav;
    }

}
