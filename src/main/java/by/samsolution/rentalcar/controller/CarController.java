package by.samsolution.rentalcar.controller;

import by.samsolution.rentalcar.entity.Car;
import by.samsolution.rentalcar.service.CarClassService;
import by.samsolution.rentalcar.service.CarService;
import by.samsolution.rentalcar.service.CrudService;
import by.samsolution.rentalcar.service.EngineService;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Map;


@Controller
@RequestMapping("/car")
public class CarController extends BaseController<Car> {

    public static final Logger LOG = Logger.getLogger(CarController.class);

    @Resource
    private CarService carService;
    @Resource
    private EngineService engineService;
    @Resource
    private CarClassService carClassService;
    @Resource
    private Environment env;


    @RequestMapping(value = "/carInfo/{id}")
    public ModelAndView getCarInfo(@PathVariable Integer id) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName(getName() + "Info");
        Car car = carService.findById(id);
        mav.addObject("car", car);
        return mav;
    }

    @RequestMapping(value = "/byClass/{id}")
    public ModelAndView getCarsByClass(@PathVariable Integer id, @RequestParam Map<String, String> allRequestParams) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName(getName() + "List");
        Integer page = Integer.parseInt(allRequestParams.get("page")) - 1;
        Page<Car> cars = carService.getCarsByClass(id, page);
        mav.addObject("page", cars);
        return mav;
    }


    @RequestMapping(value = "/image/{id}")
    public void displayCarImage(@PathVariable Integer id, HttpServletResponse response) throws IOException {
        response.setContentType("image/jpeg");
        BufferedOutputStream out = null;
        String serverPath = env.getProperty("path.upload.folder");
        String dbPath = carService.findById(id).getImagePath();
        try {
            FileInputStream fis = new FileInputStream(new File(serverPath + dbPath));
            BufferedInputStream bis = new BufferedInputStream(fis);
            out = new BufferedOutputStream(response.getOutputStream());
            int data;
            while ((data = bis.read()) != -1) {
                out.write(data);
            }
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    @Override
    protected String getName() {
        return "car/car";
    }

    @Override
    protected CrudService<Car> getCrudService() {
        return carService;
    }

    @Override
    protected Car buildNewEntity(@RequestParam Map<String, String> allRequestParams) {
        return new Car();
    }

    @Override
    protected void addComponentsFromStorage(ModelAndView mav) {
        mav.addObject("engineList", engineService.getAllEngines());
        mav.addObject("carClassList", carClassService.getAllCarClasses());
    }

    @Override
    protected boolean uploadFile(MultipartFile file, Car car, ModelAndView mav) {
        if (file.getSize() > 0) {
            if (file.getContentType().equals("image/jpeg") || file.getContentType().equals("image/png")) {
                try {
                    byte[] bytes = file.getBytes();
                    String extension = FilenameUtils.getExtension(file.getOriginalFilename());
                    String dbPath = "uploads/cars/" + car.getMark() + System.currentTimeMillis() + "." + extension;
                    String serverPath = env.getProperty("path.upload.folder");
                    car.setImagePath(dbPath);
                    File savedFile = new File(serverPath + dbPath);
                    BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(savedFile));
                    out.write(bytes);
                    out.close();
                    LOG.info("Server File Location="
                            + savedFile.getAbsolutePath());
                    return true;
                } catch (Exception e) {
                    LOG.info("File upload failed");
                    mav.setViewName(getName() + "Save");
                    mav.addObject("uploadError", "File upload error");
                    return false;
                }
            } else {
                mav.setViewName(getName() + "Save");
                mav.addObject("uploadError", "file.not.jpeg.or.png");
                return false;
            }
        }
        return true;
    }


}
