package by.samsolution.rentalcar.controller;

import by.samsolution.rentalcar.entity.User;
import by.samsolution.rentalcar.entity.UserDetailsCustom;
import by.samsolution.rentalcar.entity.enumeration.AccessLevel;
import by.samsolution.rentalcar.exception.BusinessException;
import by.samsolution.rentalcar.exception.LoginAlreadyExistException;
import by.samsolution.rentalcar.service.CrudService;
import by.samsolution.rentalcar.service.UserService;
import by.samsolution.rentalcar.validator.ConfirmPasswordValidator;
import org.apache.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.Map;

@Controller
@RequestMapping("/user")
public class UserController extends BaseController<User> {
    private final static Logger LOG = Logger.getLogger(UserController.class);

    @Resource
    private UserService userService;
    @Resource
    private ConfirmPasswordValidator confirmPasswordValidator;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView loginPage() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName(getName() + "Login");
        return mav;
    }


    @RequestMapping(value = "/login-success", method = RequestMethod.GET)
    public ModelAndView successLogin() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("redirect:/");
        return mav;
    }

    @RequestMapping(value = "/login-error", method = RequestMethod.GET)
    public ModelAndView invalidLogin() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName(getName() + "Login");
        mav.addObject("messageWrongPass", "Wrong.login.or.password");
        return mav;
    }

    @Override
    @RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
    public ModelAndView updateEntityPage(@PathVariable Integer id) {
        ModelAndView mav = new ModelAndView();
        UserDetailsCustom principal = (UserDetailsCustom) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!principal.getId().equals(id) && !principal.getAccessLevel().equals(AccessLevel.ADMIN)) {
            LOG.info("Attempt to get access to user id:" + id + " from user id:" + principal.getId());
            mav.setViewName("error/error");
            return mav;
        } else {
            return super.updateEntityPage(id);
        }
    }


    @Override
    protected String getName() {
        return "user/user";
    }

    @Override
    protected CrudService<User> getCrudService() {
        return userService;
    }

    @Override
    protected User buildNewEntity(@RequestParam Map<String, String> allRequestParams) {
        return new User();
    }

    @Override
    protected ModelAndView processException(BusinessException e, User user) {
        ModelAndView mav = new ModelAndView();
        if (e instanceof LoginAlreadyExistException) {
            mav.setViewName(getName() + "Save");
            mav.addObject("loginAlreadyExist", "login.already.exists");
        }
        return mav;
    }

    @Override
    protected void validateEntity(User user, BindingResult bindingResult) {
        confirmPasswordValidator.validate(user, bindingResult);
    }
}
