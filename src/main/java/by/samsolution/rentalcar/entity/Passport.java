package by.samsolution.rentalcar.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "passport_data")
public class Passport implements IEntity {

    public static final String DATE_PATTERN = "dd.MM.yyyy";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_passport")
    private Integer id;

    @Column(name = "series_and_number")
    @Size(min = 5, max = 15)
    private String seriesAndNumber;

    @Column(name = "date_of_giving")
    @DateTimeFormat(pattern = DATE_PATTERN)
    @NotNull
    @Past
    private Date givingDate;

    @Column(name = "date_of_expiry")
    @DateTimeFormat(pattern = DATE_PATTERN)
    @NotNull
    @Future
    private Date expiryDate;

    @Column(name = "issued_by")
    @Size(min = 5, max = 40)
    private String issuedBy;

    public Passport() {
    }

    public Passport(int id, String seriesAndNumber, Date givingDate, Date expiryDate, String issuedBy) {
        this.id = id;
        this.seriesAndNumber = seriesAndNumber;
        this.givingDate = givingDate;
        this.expiryDate = expiryDate;
        this.issuedBy = issuedBy;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSeriesAndNumber() {
        return seriesAndNumber;
    }

    public void setSeriesAndNumber(String seriesAndNumber) {
        this.seriesAndNumber = seriesAndNumber;
    }

    public Date getGivingDate() {
        return givingDate;
    }

    public void setGivingDate(Date givingDate) {
        this.givingDate = givingDate;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getIssuedBy() {
        return issuedBy;
    }

    public void setIssuedBy(String issuedBy) {
        this.issuedBy = issuedBy;
    }


}
