package by.samsolution.rentalcar.entity.enumeration;

/**
 * Created by yaza on 22.07.2014.
 */
public enum ActiveCarStatus {
    ACTIVE,
    INACTIVE
}
