package by.samsolution.rentalcar.entity.enumeration;


public enum OrderStatus {
    PENDING,
    CONFIRMED,
    DECLINED
}
