package by.samsolution.rentalcar.entity;

import by.samsolution.rentalcar.entity.enumeration.OrderStatus;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "\"order\"")
public class Order implements IEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_order")
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_car")
    private Car car;

    @Column(name = "date_of_order")
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date dateOrder;

    @Column(name = "date_in")
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    @NotNull
    private Date dateIn;

    @Column(name = "date_out")
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    @NotNull
    private Date dateOut;

    @Column(name = "order_status")
    @Enumerated(EnumType.STRING)
    private OrderStatus orderStatus;

    @Column(name = "totalCost")
    private Double totalCost;

    @Version
    @Column(name = "version")
    private int version;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Date getDateOrder() {
        return dateOrder;
    }

    public void setDateOrder(Date dateOrder) {
        this.dateOrder = dateOrder;
    }

    public Date getDateIn() {
        return dateIn;
    }

    public void setDateIn(Date dateIn) {
        this.dateIn = dateIn;
    }

    public Date getDateOut() {
        return dateOut;
    }

    public void setDateOut(Date dateOut) {
        this.dateOut = dateOut;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Double totalCost) {
        this.totalCost = totalCost;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", user=" + user.getId() +
                ", car=" + car.getId() +
                ", dateOrder=" + dateOrder +
                ", dateIn=" + dateIn +
                ", dateOut=" + dateOut +
                ", orderStatus=" + orderStatus +
                ", totalCost=" + totalCost +
                '}';
    }
}
