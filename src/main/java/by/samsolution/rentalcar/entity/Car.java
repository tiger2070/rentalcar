package by.samsolution.rentalcar.entity;

import by.samsolution.rentalcar.entity.enumeration.ActiveCarStatus;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "car")
public class Car implements IEntity, Serializable {
    public final static String YEAR_REGEXP = "^(19|20)\\d{2}$";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_car")
    private Integer id;

    @Column(name = "year")
    @Pattern(regexp = YEAR_REGEXP)
    private String year;

    @Column(name = "mark")
    @Size(min = 2, max = 100)
    private String mark;

    @Column(name = "model")
    @Size(min = 2, max = 100)
    private String model;

    @Column(name = "color")
    @Size(min = 2, max = 100)
    private String color;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_engine")
    private Engine engine;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_class")
    private CarClass carClass;


    @Enumerated(EnumType.STRING)
    @Column(name = "active_car_status")
    private ActiveCarStatus activeStatus;

    @Column(name = "cost_per_day")
    private Double costPerDay;

    @Column(name = "image_path")
    private String imagePath;

    @Version
    @Column(name = "version")
    private int version;

    public Car() {

    }

    public Car(Integer id, String year, String mark, String model, String color,
               Engine engine, CarClass carClass, ActiveCarStatus activeStatus) {
        this.id = id;
        this.year = year;
        this.mark = mark;
        this.model = model;
        this.color = color;
        this.engine = engine;
        this.carClass = carClass;
        this.activeStatus = activeStatus;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public CarClass getCarClass() {
        return carClass;
    }

    public void setCarClass(CarClass carClass) {
        this.carClass = carClass;
    }

    public ActiveCarStatus getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(ActiveCarStatus activeStatus) {
        this.activeStatus = activeStatus;
    }

    public Double getCostPerDay() {
        return costPerDay;
    }

    public void setCostPerDay(Double costPerDay) {
        this.costPerDay = costPerDay;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", year='" + year + '\'' +
                ", mark='" + mark + '\'' +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", engine=" + engine.getEngineType() +
                ", carClass=" + carClass.getClassType() +
                ", activeStatus=" + activeStatus +
                ", costPerDay=" + costPerDay +
                '}';
    }
}