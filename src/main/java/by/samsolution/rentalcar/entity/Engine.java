package by.samsolution.rentalcar.entity;

import javax.persistence.*;

@Entity
@Table(name = "engine")

public class Engine implements IEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_engine")
    private Integer id;

    @Column(name = "engine_type")
    private String engineType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEngineType() {
        return engineType;
    }

    public void setEngineType(String engineType) {
        this.engineType = engineType;
    }
}
