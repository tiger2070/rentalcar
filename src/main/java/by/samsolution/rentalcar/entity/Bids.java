package by.samsolution.rentalcar.entity;

import java.util.List;

public class Bids {
    private List<Order> orders;

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

}
