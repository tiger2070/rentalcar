CREATE DATABASE  IF NOT EXISTS `rentalcar` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `rentalcar`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: rentalcar
-- ------------------------------------------------------
-- Server version	5.6.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `car`
--

DROP TABLE IF EXISTS `car`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car` (
  `id_car` int(11) NOT NULL AUTO_INCREMENT,
  `id_class` int(11) NOT NULL,
  `mark` varchar(45) NOT NULL,
  `model` varchar(45) NOT NULL,
  `year` int(11) NOT NULL,
  `color` varchar(45) NOT NULL,
  `id_engine` int(11) NOT NULL,
  `active_car_status` varchar(45) NOT NULL,
  `cost_per_day` float DEFAULT NULL,
  `image_path` varchar(200) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_car`),
  UNIQUE KEY `id_UNIQUE` (`id_car`),
  KEY `id_engine_idx` (`id_engine`),
  KEY `id_class_idx` (`id_class`),
  CONSTRAINT `id_class` FOREIGN KEY (`id_class`) REFERENCES `car_class` (`id_class`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `id_engine` FOREIGN KEY (`id_engine`) REFERENCES `engine` (`id_engine`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car`
--

LOCK TABLES `car` WRITE;
/*!40000 ALTER TABLE `car` DISABLE KEYS */;
INSERT INTO `car` VALUES (2,2,'BMW','X6',2013,'Blue',1,'ACTIVE',979000,'uploads/cars/BMW1410121029422.jpg',10),(3,1,'Audi','A7',2014,'Brown',2,'ACTIVE',998000,'uploads/cars/Audi1410121129656.jpg',1),(4,1,'Porshe','Panamera',2012,'Brown',2,'ACTIVE',1350000,'uploads/cars/Porshe1410122636560.jpg',1),(5,6,'Lamborghini','Aventador',2014,'Red',1,'ACTIVE',4225000,'uploads/cars/Lamborghini1410121437367.jpg',1),(6,1,'Tesla','Model S',2014,'White',4,'ACTIVE',1500000,'uploads/cars/Tesla1410121489232.jpg',1),(7,3,'Toyota','Prius',2011,'Silver',5,'INACTIVE',564000,'uploads/cars/Toyota1410121553511.jpg',1),(8,5,'RangeRover','Evogue',2014,'White',2,'ACTIVE',915000,'uploads/cars/RangeRover1410121646489.jpg',1),(9,6,'Audi','R8',2011,'Gold',1,'ACTIVE',1834000,'uploads/cars/Audi1410121748433.jpg',1),(10,6,'Bugatti','Veyron',2013,'Silver',1,'ACTIVE',10630000,'uploads/cars/Bugatti1410121922118.jpg',1),(11,4,'Mini Cooper','R53',2011,'Blue',3,'ACTIVE',1320000,'uploads/cars/Mini Cooper1410122750678.jpg',1),(12,6,'BMW','i8',2014,'Blue',5,'ACTIVE',5778000,'uploads/cars/BMW1410126260908.jpg',1),(13,6,'Ferrari','F12 Berlinetta',2013,'Red',1,'ACTIVE',9830000,'uploads/cars/Ferrari1410122266383.jpg',1),(14,1,'Mercedes-Benz','CLS',2014,'Red',1,'ACTIVE',6582000,'uploads/cars/Mercedes-Benz1410122370974.jpg',1),(15,1,'Infinity','G37',2010,'Red',1,'ACTIVE',3200000,'uploads/cars/Infinity1410122427890.jpg',1),(19,1,'Jaguar','XF',2012,'Green',1,'ACTIVE',1524000,'uploads/cars/Jaguar1410122475697.jpg',1),(41,6,'Nissan','GT-R',2010,'Silver',1,'ACTIVE',1469000,'uploads/cars/Nissan1410126145301.jpg',1),(42,4,'Audi','TT',2013,'Blue',1,'ACTIVE',1832000,'uploads/cars/Audi1410126578038.jpg',1),(64,5,'Jeep','Grand Cherokee SRT-8',2014,'Red',1,'ACTIVE',3869000,'uploads/cars/Jeep1410868145205.jpg',1);
/*!40000 ALTER TABLE `car` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_class`
--

DROP TABLE IF EXISTS `car_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car_class` (
  `id_class` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(45) NOT NULL,
  PRIMARY KEY (`id_class`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car_class`
--

LOCK TABLES `car_class` WRITE;
/*!40000 ALTER TABLE `car_class` DISABLE KEYS */;
INSERT INTO `car_class` VALUES (1,'business'),(2,'family'),(3,'medium'),(4,'small'),(5,'suv'),(6,'sport');
/*!40000 ALTER TABLE `car_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `engine`
--

DROP TABLE IF EXISTS `engine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `engine` (
  `id_engine` int(11) NOT NULL AUTO_INCREMENT,
  `engine_type` varchar(45) NOT NULL,
  PRIMARY KEY (`id_engine`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `engine`
--

LOCK TABLES `engine` WRITE;
/*!40000 ALTER TABLE `engine` DISABLE KEYS */;
INSERT INTO `engine` VALUES (1,'bensin'),(2,'diesel'),(3,'gas'),(4,'electro'),(5,'hybrid');
/*!40000 ALTER TABLE `engine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `id_order` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_car` int(11) NOT NULL,
  `order_status` varchar(45) DEFAULT NULL,
  `date_of_order` datetime DEFAULT NULL,
  `date_in` date NOT NULL,
  `date_out` date NOT NULL,
  `totalCost` double NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_order`),
  KEY `id_auto_idx` (`id_car`),
  KEY `id_user_idx` (`id_user`),
  CONSTRAINT `id_auto` FOREIGN KEY (`id_car`) REFERENCES `car` (`id_car`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (7,1,5,'CONFIRMED','2014-08-11 13:36:05','2014-09-21','2014-09-23',8450000,0),(8,2,2,'DECLINED','2014-08-11 14:12:22','2014-09-21','2014-09-22',989000,0),(10,1,5,'DECLINED','2014-08-13 13:23:53','2014-09-25','2014-09-29',16900000,0),(26,1,12,'CONFIRMED','2014-09-04 16:42:10','2014-09-21','2014-09-22',5778000,0),(27,1,13,'PENDING','2014-09-04 18:02:25','2014-09-25','2014-09-26',9830000,0),(28,1,14,'PENDING','2014-09-04 18:06:30','2014-09-21','2014-09-22',6582000,0),(29,1,15,'PENDING','2014-09-05 11:49:42','2014-09-21','2014-09-30',28800000,0),(30,1,13,'PENDING','2014-09-05 11:57:49','2014-09-21','2014-09-22',9830000,0),(31,1,10,'PENDING','2014-09-05 12:11:45','2014-09-21','2014-09-22',10630000,0),(32,1,3,'CONFIRMED','2014-09-05 12:24:41','2014-09-06','2014-09-13',6986000,0),(33,1,15,'DECLINED','2014-09-05 12:51:52','2014-09-08','2014-09-09',3200000,0),(34,2,13,'PENDING','2014-09-05 13:53:21','2014-10-05','2014-10-15',98300000,0),(35,2,2,'PENDING','2014-09-08 11:16:20','2014-09-19','2014-09-23',3956000,0),(36,1,19,'PENDING','2014-09-11 11:30:42','2014-09-19','2014-09-22',4572000,0),(37,2,2,'PENDING','2014-09-12 12:47:55','2014-10-20','2014-10-21',989000,0),(38,1,12,'PENDING','2014-09-12 13:02:42','2014-09-19','2014-09-20',5778000,0),(39,1,10,'PENDING','2014-09-15 11:38:45','2014-09-18','2014-09-19',10630000,0),(40,1,3,'PENDING','2014-09-15 18:13:04','2014-10-10','2014-10-13',2994000,1);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passport_data`
--

DROP TABLE IF EXISTS `passport_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `passport_data` (
  `id_passport` int(11) NOT NULL AUTO_INCREMENT,
  `series_and_number` varchar(45) NOT NULL,
  `date_of_giving` date NOT NULL,
  `date_of_expiry` date NOT NULL,
  `issued_by` varchar(45) NOT NULL,
  PRIMARY KEY (`id_passport`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passport_data`
--

LOCK TABLES `passport_data` WRITE;
/*!40000 ALTER TABLE `passport_data` DISABLE KEYS */;
INSERT INTO `passport_data` VALUES (1,'MP2137022','2006-06-15','2015-06-15','Октябрьский РУВД г. Минска'),(2,'MP2312312','2006-02-02','2015-02-02','рувд №4'),(3,'MP2312312','2006-02-02','2015-02-02','рувд №4'),(4,'371A81402','2005-05-05','2015-05-05','Рувд №31, Москва'),(5,'d934dfdfdf','1970-01-01','2015-02-02','dasdad'),(9,'MP2137022','2006-06-15','2015-06-15','Октябрьский РУВД '),(10,'222222222222','1970-01-01','2015-01-01','USA Texas'),(11,'MP2137022','2006-06-15','2015-06-15','Октябрьский РУВД '),(12,'MP2137022','2006-06-15','2015-06-15','Октябрьский РУВД '),(13,'MP2137022','2006-06-15','2015-06-15','Октябрьский РУВД '),(17,'3199g','2006-06-15','2015-06-15','dajdkad'),(19,'MP2137022','2006-06-15','2015-06-15','Октябрьский РУВД '),(20,'MP2137022','2006-06-15','2015-06-15','Октябрьский РУВД '),(21,'MP2137022','2006-06-15','2015-06-15','Октябрьский РУВД '),(22,'MP2137022','2006-06-15','2015-06-15','Октябрьский РУВД '),(23,'MP2137022','2006-06-15','2015-06-15','Октябрьский РУВД '),(24,'jDbC123E','1970-01-01','2015-01-01','CAO, Ontario'),(25,'dadsdasd','2006-06-15','2313-12-12','sdadasd'),(26,'MP2137022','2006-06-15','2015-06-15','Октябрьский РУВД '),(27,'MP2137022','2006-06-15','2015-06-15','Октябрьский РУВД '),(30,'MP2137022','2006-06-15','2015-06-15','Октябрьский РУВД '),(31,'MP2137022','2006-06-15','2015-06-15','Октябрьский РУВД '),(32,'Oracle','2006-06-15','2015-06-15','Oracle'),(33,'MP2137022','2006-06-15','2015-06-15','Октябрьский РУВД '),(34,'MP2312312','2006-02-02','2015-02-02','рувд №4'),(35,'dadsdasd','2006-06-15','2313-12-12','sdadasd'),(36,'Oracle','2006-06-15','2015-06-15','Oracle'),(37,'Oracle','2006-06-15','2015-06-15','Oracle'),(38,'MP2137022','2006-06-15','2015-06-15','Октябрьский РУВД '),(39,'MP2137022','2006-06-15','2015-06-15','Октябрьский РУВД '),(40,'Oracle','2006-06-15','2015-06-15','Oracle'),(41,'Oracle','2006-06-15','2015-06-15','Oracle');
/*!40000 ALTER TABLE `passport_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(45) NOT NULL,
  `password` varchar(60) NOT NULL,
  `email` varchar(45) NOT NULL,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `access_level` varchar(45) DEFAULT NULL,
  `id_passport` int(11) NOT NULL,
  `date_of_birth` date NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  KEY `id_role_idx` (`access_level`),
  KEY `id_passport_idx` (`id_passport`),
  CONSTRAINT `id_passport` FOREIGN KEY (`id_passport`) REFERENCES `passport_data` (`id_passport`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admins','$2a$10$3IutpHxSGpsUgkxEkor4HuCX4aHZF1CkS1SdTNCueyAZKAZVUPdGC','bla@bla.bla','Admin','AdminLastName','+375295597178','ADMIN',39,'1990-07-02',5),(2,'client1','$2a$10$C6vtIIfmJuTsgtvuy3cxAuF4uSmAwr1fVI33kd0YUPvMvgFJmf.Km','client1@tut.by','Client1','Client1_lastname','+375290000000','CLIENT',34,'1990-12-20',2),(3,'client2','$2a$10$Ll8kMJz25Yvch3wjqLinhORaWOGDRK59EQjvqk4leyX7JCMBAbgv2','client2@tut.by','Client2','Client2_lastname','+375299292912','CLIENT',4,'2000-02-02',1),(5,'client3','$2a$10$rWuxGuL43jJUebtqxJ1FkuOREdlSCAT0mW4wDJ257FXxAf3Ek0Uta','client3@may.be','Rolling','Stones','+000000000000','CLIENT',10,'1968-12-19',1),(7,'neoneo1','$2a$10$0rmIOBzKqwSh4y2s8ESYUOI/seePOOwwFt1jLODKIENoMNBfwNMLq','dajk@fak.da','dasda','dadsda','+375290000000','CLIENT',17,'2002-02-02',1),(8,'enumer','$2a$10$inmPAKp1H2DTMNQHhW.SQ.sy1oBmqom/bCTkEk0dCo9RS0HSq4m7i','enum@er.by','Qwer','Rty','+375290000000','CLIENT',24,'2002-02-02',1),(9,'qwerty','$2a$10$NOst1hz1vJek6GwjMIdCdepi6okwkjxCT0WYyIv6sDibO3MKRDJ/S','were@da.da','sdad','sdsda','+375290000000','CLIENT',35,'1990-12-20',2),(13,'Oracle','$2a$10$kJpf81avrQeyK7a3Z5EicOHwv8lr9h0n2azf1D1/aligBKaGMrCKS','dadh@dad.da','OracleOracle','Oracle','+375290000000','CLIENT',41,'1990-12-20',3);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-09-16 14:52:07
